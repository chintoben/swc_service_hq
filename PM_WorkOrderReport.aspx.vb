﻿Imports System.Data.SqlClient

Public Class PM_WorkOrderReport
    Inherits System.Web.UI.Page
    Dim UserID As String
    Dim db_SWC As New Connect_Service
    Dim sql1, sql As String
    Dim dt, dt1, dt2, dt3, dtt As New DataTable
    Dim db_HR As New Connect_HR

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UserID = Request.QueryString("UserID")

        'loaddataGrid()

        If Not IsPostBack Then

            If UserID <> "" Then
                sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
                sql1 += " Where  [employee_id] = '" & UserID & "'"
                dt1 = db_HR.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    lblUserName.Text = dt1.Rows(0)("fullname")
                End If

            End If

            loaddataGrid()
        End If
    End Sub

    Sub loaddataGrid()

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(db_SWC.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.4.20;initial catalog=DB_SwcService;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try

                constr = "data source=10.0.4.20;initial catalog=DB_SwcService;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "select PMID	, '" & UserID & "' as UserID ,Customer_id ,CustName	,CustAdd  ,CustPhone ,[Item], [SerialNumber], [ClassisNumber]  ,Receptionnist ,Receptionnist_Name ,PMDate  ,Technician  ,PM_Status " & vbCrLf
            sql = sql & " from [VW_PM] "
            sql = sql & " where 1=1 " & vbCrLf
            'sql = sql & " and [CreateBy] =  '" & UserID & "' " & vbCrLf

            If drpCust.SelectedValue <> "1" Then
                sql = sql & " and Customer_id like '%" & drpCust.SelectedValue & "%' "
            End If

            If Trim(txtDatePM.Text) <> "" Then
                sql = sql & " and PMDate >= '" & DateTime.Parse(txtDatePM.Text).ToString("yyyy-MM-dd") & "'" ' 00:00:00'"
            End If
            If Trim(txtDatePM2.Text) <> "" Then
                sql = sql & " and PMDate <= '" & DateTime.Parse(txtDatePM2.Text).ToString("yyyy-MM-dd") & "'" ' 23:59:59'"
            End If

            sql = sql & " order by [PMID] "

            Dim dt As New DataTable
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView.DataSource = dt
                GridView.DataBind()
            Else
                ShowNoResultFound(dt, GridView)
            End If

        Catch

        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub

    Protected Sub GridView_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView.RowDataBound


        ' CHECK IF ITS A ROW. ELSE THE HEADER WILL BE TREATED AS A ROW.
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)")
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)")
        End If
    End Sub

    Protected Sub drpCust_Init(sender As Object, e As EventArgs) Handles drpCust.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [CustomerId] as id,[CustName] from [VW_CustomerSale] group by   CustomerId , CustName order by CustomerId "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpCust.Items.Clear()
        drpCust.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpCust.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    'Protected Sub drpCust_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpCust.SelectedIndexChanged
    '    sql1 += " SELECT  [CustAdd]  ,[CustPhone],CustName  FROM [VW_CustomerSale] "
    '    sql1 += " Where  [CustomerId] = '" & drpCust.SelectedValue & "'"
    '    dt1 = db_SWC.GetDataTable(sql1)
    '    If dt1.Rows.Count > 0 Then
    '        lblCustName.Text = dt1.Rows(0)("CustName")
    '    End If
    'End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        loaddataGrid()
    End Sub


    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim sqlReport As String = " 1=1 "

        ''{VW_CM.CMID}="CM17000005"
        'If txtCMID.Text <> "" Then
        '    sqlReport = sqlReport & " and {VW_CM.CMID} = '" & txtCMID.Text & "' "
        'End If

        If drpCust.SelectedValue <> "1" Then
            sqlReport = sqlReport & " and {ReportData.Customer_id} = '" & drpCust.SelectedValue & "' "
        End If

        If Trim(txtDatePM.Text) <> "" Then
            sqlReport = sqlReport & " and {ReportData.PMDate} >= '" & DateTime.Parse(txtDatePM.Text).ToString("yyyy-MM-dd") & "'" ' 00:00:00'"
        End If
        If Trim(txtDatePM2.Text) <> "" Then
            sqlReport = sqlReport & " and {ReportData.PMDate} <= '" & DateTime.Parse(txtDatePM2.Text).ToString("yyyy-MM-dd") & "'" ' 23:59:59'"
        End If


        Server.Transfer("CM_LoadReport.aspx?Form=PMExcel&sql=" & sqlReport)
    End Sub

    Protected Sub GridView_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView.SelectedIndexChanged

    End Sub
End Class