﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Web.CrystalReportViewerBase
Imports System.Data.OleDb
Imports System.IO
Imports CrystalDecisions.ReportSource
Imports System.Web.UI.WebControls.Table

Public Class RepairOrderReportExcel
    Inherits System.Web.UI.Page
    Dim db As New Connect_HR
    Dim ids As String
    Dim uid As String
    Dim uapp As String
    Dim usr() As String
    Dim sql As String
    Dim dt As New DataTable
    Dim sess As String
    Dim sqlPara1 As SqlParameter
    Dim sqlPara2 As SqlParameter
    Dim fname As String
    Dim Formula As FormulaFieldDefinition
    Dim sqlReport, Form As String
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table
        Dim cryRpt As New ReportDocument

        Form = Request.QueryString(0)
        sqlReport = Request.QueryString(1)

        If Form = "Excel" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_RepairOrderExcel.rpt"))
            cryRpt.Load(Server.MapPath("RPT_RepairOrderExcel.rpt"))
   
        ElseIf Form = "PMExcel" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_WorkOrderExcel.rpt"))
            cryRpt.Load(Server.MapPath("RPT_WorkOrderExcel.rpt"))
      
        End If


        Call CreateCondition()
        If sql <> "" Then
            cryRpt.RecordSelectionFormula = sql
        End If


        With crConnectionInfo
            .ServerName = "10.0.4.20"
            .DatabaseName = "DB_SwcService"
            .UserID = "armth"
            .Password = "Kb5r#Ge9Z3M*mQ"
        End With


        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

     If Form = "Excel" Then
            CrystalReportViewer1.ID = "RepairOrder_" & Date.Now.ToString("dd/MM/yyyy")

        ElseIf Form = "PMExcel" Then
            CrystalReportViewer1.ID = "PMExcel_" & Date.Now.ToString("dd/MM/yyyy")

        End If

        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.RefreshReport()

    End Sub

    Sub CreateCondition()
        If sqlReport <> "" Then
            sql = sqlReport
        End If

    End Sub

End Class