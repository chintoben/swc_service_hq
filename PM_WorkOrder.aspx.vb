﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class PM_WorkOrder
    Inherits System.Web.UI.Page
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim sql1, sql, sql3 As String
    Dim dt, dt1, dt2, dt3, dtt As New DataTable
    Dim UserID, PMID As String
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        UserID = Request.QueryString("UserID")
        PMID = Request.QueryString("PMID")


        If Not IsPostBack Then
            txtDateWork.Text = Date.Now.ToString("dd/MM/yyyy")
            'TB_PM50Hr.Visible = False

            If UserID <> "" Then
                sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
                sql1 += " Where  [employee_id] = '" & UserID & "'"
                dt1 = db_HR.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    lblUserName.Text = dt1.Rows(0)("fullname")
                End If

            End If

            'Check New  หรือ old 
            sql += " SELECT * FROM [TB_PreventiveMaintenance] "
            sql += " Where  PMID = '" & PMID & "'"
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count <= 0 Then
                'new
                GenID()
                btnCreateWorkOrder.Visible = True
                btnSave.Visible = False
                btnDelete.Visible = False
                btnReport.Visible = False
            Else
                txtPMID.Text = PMID
                txtPMID.Enabled = False
                btnCreateWorkOrder.Visible = False
                btnSave.Visible = True
                btnDelete.Visible = True
                btnReport.Visible = True
                loadPM(PMID)

                If dt.Rows(0)("PM_Type") = "0" Then
                    drpPMType.Enabled = True
                Else
                    drpPMType.Enabled = False
                End If



            End If

        End If
    End Sub

    Sub loadPM(PMID)
        sql = " SELECT PMID	 ,Customer_id   ,CustName	      ,CustAdd	      ,CustPhone	      ,item	      ,Serialnumber ,	ClassisNumber      ,Receptionnist "
        sql += " ,PMDate  ,Technician   ,PM_Type    ,PM_Status	      ,CreateBy	      ,CreateDate	      ,ModifyBy	      ,ModifyDate"
        sql += " from [VW_PM] "
        sql += " where PMID = '" & PMID & "'"

        dt = db_SWC.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtPMID.Text = dt.Rows(0)("PMID")
            txtDateWork.Text = dt.Rows(0)("PMDate")
            drpStatus.SelectedValue = dt.Rows(0)("PM_Status")
            drpPMType.SelectedValue = dt.Rows(0)("PM_Type")
            drpReceptionnist.SelectedValue = dt.Rows(0)("Receptionnist")
            drpTechnician.SelectedValue = dt.Rows(0)("Technician")
            drpCust.SelectedValue = dt.Rows(0)("Customer_id")
            drpItem.SelectedValue = dt.Rows(0)("item")
            drpSerialNumber.SelectedValue = dt.Rows(0)("Serialnumber")
            drpClassisNumber.SelectedValue = dt.Rows(0)("ClassisNumber")
            lblCustName.Text = dt.Rows(0)("CustName")
            lblCustAdd.Text = dt.Rows(0)("CustAdd")
            lblCustTel.Text = dt.Rows(0)("CustPhone")


            Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
            Dim sqlCmd As New SqlCommand
            Dim Rs As SqlDataReader
            Dim sql As String

            sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale]  group by [ItemId] ,[ItemDesc]"

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpItem.Items.Clear()
            drpItem.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            End While

            '  drpItem.SelectedValue = dt.Rows(0)("item")


            Dim sqlCon2 As New SqlConnection(db_SWC.sqlCon2)
            Dim sqlCmd2 As New SqlCommand
            Dim Rs2 As SqlDataReader
            Dim sql2 As String

            sql2 = "select  [SerialNumber] as id  from [VW_CustomerSale]  "

            If sqlCon2.State = ConnectionState.Closed Then
                sqlCon2 = New SqlConnection(db_SWC.sqlCon)
                sqlCon2.Open()
            End If
            With sqlCmd2
                .Connection = sqlCon2
                .CommandType = CommandType.Text
                .CommandText = sql2
                Rs2 = .ExecuteReader
            End With
            drpSerialNumber.Items.Clear()
            drpSerialNumber.Items.Add(New ListItem("", 1))
            While Rs2.Read
                drpSerialNumber.Items.Add(New ListItem(Rs2.GetString(0), Rs2.GetString(0)))
            End While

            'drpSerialNumber.SelectedValue = dt.Rows(0)("Sale_ItemID")


            Dim sqlCon3 As New SqlConnection(db_SWC.sqlCon2)
            Dim sqlCmd3 As New SqlCommand
            Dim Rs3 As SqlDataReader
            Dim sql3 As String
            sql3 = "select  [ChassisNumber] as id  from [VW_CustomerSale]  "

            If sqlCon3.State = ConnectionState.Closed Then
                sqlCon3 = New SqlConnection(db_SWC.sqlCon)
                sqlCon3.Open()
            End If
            With sqlCmd3
                .Connection = sqlCon3
                .CommandType = CommandType.Text
                .CommandText = sql3
                Rs3 = .ExecuteReader
            End With
            drpClassisNumber.Items.Clear()
            drpClassisNumber.Items.Add(New ListItem("", 1))
            While Rs3.Read
                drpClassisNumber.Items.Add(New ListItem(Rs3.GetString(0), Rs3.GetString(0)))
            End While


        End If

    End Sub


    Protected Sub btnCreateWorkOrder_Click(sender As Object, e As EventArgs) Handles btnCreateWorkOrder.Click
        If txtPMID.Enabled = True Then
            GenID()

            Dim sql As String = "INSERT INTO [TB_PreventiveMaintenance] (PMID,[Customer_id] ,[Item],[SerialNumber],[ClassisNumber] "
            sql &= ",Receptionnist,Technician ,PMDate,PM_Type , PM_Status, CreateBy, CreateDate"
            ' sql &= ",Technician ,Inspector ,Consignee"
            sql &= ")"
            sql &= "VALUES ('" & txtPMID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & drpTechnician.SelectedValue & "',getdate() "
            sql &= ",'" & drpPMType.SelectedValue & "','" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "',getdate())"
            dtt = db_SWC.GetDataTable(sql)

            txtPMID.Enabled = False
            btnCreateWorkOrder.Visible = False
            btnSave.Visible = True
            btnDelete.Visible = True
            btnReport.Visible = True
        Else
            'Update
            UpdatePM()
        End If
    End Sub



    Sub GenID()

        If txtPMID.Enabled = True Then
            ' New ID
            Dim conn As New Connect_Service
            Dim sql As String = "Select * from [TB_PreventiveMaintenance] where len(PMID) = 10 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([PMID],3,2)"
            Dim sql2 As String = "Select Max(Right(PMID,6)) as PMID from [TB_PreventiveMaintenance] where len(PMID) = 10 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([PMID],3,2)"

            dt = db_SWC.GetDataTable(sql)
            dt2 = db_SWC.GetDataTable(sql2)

            If dt.Rows.Count <= 0 Then
                txtPMID.Text = "PM" & Now.ToString("yy") & "000001"
            Else
                Dim newID As Integer = CInt(dt2.Rows(0)("PMID"))
                newID += 1
                txtPMID.Text = "PM" & Now.ToString("yy") & newID.ToString("000000")
            End If
        Else
        End If
    End Sub

    Sub UpdatePM()
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim sqlcon As New OleDbConnection(db_SWC.cnPAStr)
        Dim sqlcom As New OleDbCommand

        Dim sSql As String = "UPDATE [TB_PreventiveMaintenance] SET  "
        sSql += " [Customer_id]='" & drpCust.SelectedValue & "',"
        sSql += " [Item]='" & drpItem.SelectedValue & "',"
        sSql += " [SerialNumber]='" & drpSerialNumber.SelectedValue & "',"
        sSql += " [ClassisNumber]='" & drpClassisNumber.SelectedValue & "',"
        sSql += " [Receptionnist]='" & drpReceptionnist.SelectedValue & "',"
        sSql += " [Technician]='" & drpTechnician.SelectedValue & "',"
        sSql += " [PMDate]='" & DateTime.Parse(txtDateWork.Text).ToString("yyyy-MM-dd") & "',"
        sSql += " [PM_Status]='" & drpStatus.SelectedValue & "',"
        sSql += " [PM_Type]='" & drpPMType.SelectedValue & "',"
        ' drpPMType.SelectedValue = dt.Rows(0)("PM_Type")

        sSql += " ModifyBy = '" & UserID & "',"
        sSql += " ModifyDate = getdate() "

        sSql += " Where PMID = '" & txtPMID.Text & "'"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_SWC.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With
    End Sub

   

    Protected Sub drpReceptionnist_Init(sender As Object, e As EventArgs) Handles drpReceptionnist.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]   "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpReceptionnist.Items.Clear()
        drpReceptionnist.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpReceptionnist.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    'Protected Sub drpSale_Init(sender As Object, e As EventArgs)
    '    Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String

    '    sql = "select [SaleId] as id,[SaleNum]  from [VW_CustomerSale] where  SaleId is not null and [CustomerId] like '" & drpCust.SelectedValue & "' group by [SaleId] ,[SaleNum]"

    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(db_SWC.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpItem.Items.Clear()
    '    drpItem.Items.Add(New ListItem("", 1))
    '    While Rs.Read
    '        drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
    '    End While
    'End Sub

    Protected Sub drpSale_SelectedIndexChanged(sender As Object, e As EventArgs)

        '----------
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
        sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  SaleId is not null and [SaleId] like '" & drpItem.SelectedValue & "' group by [ItemId] ,[ItemDesc]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSerialNumber.Items.Clear()
        drpSerialNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub

    'Protected Sub drpSaleItem_Init(sender As Object, e As EventArgs) Handles drpSerialNumber.Init
    '    Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String

    '    'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
    '    sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  SaleId is not null and [SaleId] like '" & drpItem.SelectedValue & "' group by [ItemId] ,[ItemDesc]"

    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(db_SWC.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpSerialNumber.Items.Clear()
    '    drpSerialNumber.Items.Add(New ListItem("", 1))
    '    While Rs.Read
    '        drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
    '    End While
    'End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        UpdatePM()
    End Sub

    Protected Sub brnCheckList_Click(sender As Object, e As EventArgs) Handles brnCheckList.Click
        If drpPMType.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุประเภทการตรวจเช็ค"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        ElseIf drpPMType.SelectedValue = "50Hr" Then
            Server.Transfer("PM_WorkOrder50Hr.aspx?&PMID=" & txtPMID.Text & "&UserID=" & UserID)
        ElseIf drpPMType.SelectedValue = "100Hr" Then
            Server.Transfer("PM_WorkOrder100Hr.aspx?&PMID=" & txtPMID.Text & "&UserID=" & UserID)
        End If
    End Sub


    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        If drpPMType.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุประเภทการตรวจเช็ค"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        ElseIf drpPMType.SelectedValue = "50Hr" Then
            Dim sqlReport As String = " 1=1 "
            If PMID <> "" Then
                sqlReport = sqlReport & " and {VW_PM50.PMID} = '" & PMID & "' "
            End If
            Server.Transfer("CM_LoadReport.aspx?Form=PM50&sql=" & sqlReport)
        ElseIf drpPMType.SelectedValue = "100Hr" Then
            Dim sqlReport As String = " 1=1 "
            If PMID <> "" Then
                sqlReport = sqlReport & " and {VW_PM100.PMID} = '" & PMID & "' "
            End If
            Server.Transfer("CM_LoadReport.aspx?Form=PM100&sql=" & sqlReport)
        End If
    End Sub


    Protected Sub drpTechnician_Init(sender As Object, e As EventArgs) Handles drpTechnician.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]   "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpTechnician.Items.Clear()
        drpTechnician.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpTechnician.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim sqlcon As New OleDbConnection(db_SWC.cnPAStr)
        Dim sqlcom As New OleDbCommand

        Dim sSql As String = "UPDATE [TB_PreventiveMaintenance] SET  "
        'sSql += " [Customer_id]='" & drpCust.SelectedValue & "',"
        'sSql += " [Sale_ID]='" & drpSale.SelectedValue & "',"
        'sSql += " [Sale_ItemID]='" & drpSaleItem.SelectedValue & "',"
        'sSql += " [Receptionnist]='" & drpReceptionnist.SelectedValue & "',"
        'sSql += " [Technician]='" & drpTechnician.SelectedValue & "',"
        'sSql += " [PMDate]='" & DateTime.Parse(txtDateWork.Text).ToString("yyyy-MM-dd") & "',"
        sSql += " [PM_Status]='Delete',"
        'sSql += " [PM_Type]='" & drpPMType.SelectedValue & "',"

        sSql += " ModifyBy = '" & UserID & "',"
        sSql += " ModifyDate = getdate()"

        sSql += " Where PMID = '" & txtPMID.Text & "'"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_SWC.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With

        Server.Transfer("PM_WorkOrderReport.aspx?UserID=" & UserID)
    End Sub

    Protected Sub drpCust_Init(sender As Object, e As EventArgs) Handles drpCust.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [CustomerId] as id,[CustName] from [VW_CustomerSale] group by   CustomerId , CustName order by CustomerId "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpCust.Items.Clear()
        drpCust.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpCust.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpCust_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpCust.SelectedIndexChanged

        sql1 += " SELECT  [CustAdd]  ,[CustPhone],CustName  FROM [VW_CustomerSale] "
        sql1 += " Where  [CustomerId] = '" & drpCust.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblCustAdd.Text = dt1.Rows(0)("CustAdd")
            lblCustTel.Text = dt1.Rows(0)("CustPhone")
            lblCustName.Text = dt1.Rows(0)("CustName")
        End If

        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  [CustomerId] like '" & drpCust.SelectedValue & "' group by ItemId,ItemDesc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpItem.Items.Clear()
        drpItem.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub

    Protected Sub drpItem_Init(sender As Object, e As EventArgs) Handles drpItem.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where [CustomerId] like '" & drpCust.SelectedValue & "' "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpItem.Items.Clear()
        drpItem.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpItem.SelectedIndexChanged
        '----------
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
        sql = "select [SerialNumber] as id  from [VW_CustomerSale] where  [ItemID] like '" & drpItem.SelectedValue & "'  group by SerialNumber"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSerialNumber.Items.Clear()
        drpSerialNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpSerialNumber_Init(sender As Object, e As EventArgs) Handles drpSerialNumber.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [SerialNumber] as id  from [VW_CustomerSale] where [ItemID] like '" & drpItem.SelectedValue & "' group by SerialNumber"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If

        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSerialNumber.Items.Clear()
        drpSerialNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSerialNumber.Items.Add(New ListItem(Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpSerialNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSerialNumber.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ChassisNumber] as id  from [VW_CustomerSale] where  [SerialNumber] like '" & drpSerialNumber.SelectedValue & "'  group by ChassisNumber"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpClassisNumber.Items.Clear()
        drpClassisNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpClassisNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpClassisNumber_Init(sender As Object, e As EventArgs) Handles drpClassisNumber.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ChassisNumber] as id  from [VW_CustomerSale] where [SerialNumber] like '" & drpSerialNumber.SelectedValue & "' "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpClassisNumber.Items.Clear()
        drpClassisNumber.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpClassisNumber.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub btnAddPart_Click(sender As Object, e As EventArgs) Handles btnAddPart.Click
        If txtPMID.Enabled = True Then
            GenID()

            'Main
            Dim sql As String = "INSERT INTO [TB_PreventiveMaintenance] (PMID,[Customer_id] ,[Item],[SerialNumber],[ClassisNumber] "
            sql &= ",Receptionnist,Technician ,PMDate,PM_Type , PM_Status, CreateBy, CreateDate"
            ' sql &= ",Technician ,Inspector ,Consignee"
            sql &= ")"
            sql &= "VALUES ('" & txtPMID.Text & "','" & drpCust.SelectedValue & "','" & drpItem.SelectedValue & "','" & drpSerialNumber.SelectedValue & "','" & drpClassisNumber.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & drpTechnician.SelectedValue & "','" & DateTime.Parse(txtDateWork.Text).ToString("yyyy-MM-dd") & "' "
            sql &= ",'" & drpPMType.SelectedValue & "','" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "',getdate() )"
            dtt = db_SWC.GetDataTable(sql)

            txtPMID.Enabled = False
            btnCreateWorkOrder.Visible = False
            btnSave.Visible = True
            btnDelete.Visible = True
            btnReport.Visible = True


            'Part
            If txtQtyUse.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            Dim sql0 As String = "INSERT INTO [TB_PMPartRequired] ([PMID] ,PartNo,Description,QtyUse ,Price, CreateBy, CreateDate)"
            sql0 &= "VALUES ('" & txtPMID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "','" & UserID & "',getdate())"
            dtt = db_SWC.GetDataTable(sql0)
            'GridView2.DataBind()

        Else
            If txtQtyUse.Text = "" Then
                sms.Msg = "กรุณาระบุจำนวน !!"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If

            Dim sql As String = "INSERT INTO [TB_PMPartRequired] ([PMID] ,PartNo,Description,QtyUse ,Price, CreateBy, CreateDate)"
            sql &= "VALUES ('" & txtPMID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "','" & UserID & "',getdate())"
            dtt = db_SWC.GetDataTable(sql)

            'GridView3.DataBind()

        End If

        drpPartNo.SelectedValue = "1"
        lblDescriptionPart.Text = ""
        txtQty.Text = ""
        txtUnitPrice.Text = ""
        txtQtyUse.Text = ""
        GridView3.DataBind()
    End Sub

    Protected Sub drpPartNo_Init(sender As Object, e As EventArgs) Handles drpPartNo.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from [VW_PartsWH] ", conn) ' where SaleId = '" & drpSale.SelectedValue & "'", conn

        sql = "select [ItemId] as id,[ItemDesc]  from [VW_PartsWH] where  ItemId is not null  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpPartNo.Items.Clear()
        drpPartNo.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpPartNo.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpPartNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpPartNo.SelectedIndexChanged
        sql1 += " SELECT  [itemdesc]  ,[qty],UnitPrice  FROM [VW_PartsWH] "
        sql1 += " Where  [ItemId] = '" & drpPartNo.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblDescriptionPart.Text = dt1.Rows(0)("itemdesc")
            txtQty.Text = dt1.Rows(0)("qty")
            txtUnitPrice.Text = dt1.Rows(0)("UnitPrice")
        End If
    End Sub
End Class