﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PM_WorkOrderReport.aspx.vb" Inherits="SWC_ServiceSystem.PM_WorkOrderReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

 
 <%--TAB--%>
<%--<link rel="stylesheet" href="css/bootstrap.min.css">--%>
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>

  <script type = "text/javascript">
      function SetTarget() {
          document.forms[0].target = "_blank";
      }
</script>


    <style type="text/css">
        .style1
        {
            height: 35px;
        }
    </style>




</head>
<body>
     <form id="form1" runat="server">
 <div id="wrapper">

            <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Task Main</span>
                      </a>
                    </li>    
                       <li>
                                Corrective Maintenance

                        </li>
                       <li>
                           <a  href="CM_RepairOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form (CM)</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="CM_RepairOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Repair Order Report (CM)</span>
                      </a>

                    </li>

                   <h5>  Preventive Maintenance</h5>    
        
                    <li>
                         <a  href="PM_WorkOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>PM Form</span>
                        </a>
                    </li>
                    <li>
                          <a  href="PM_WorkOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>PM Report</span>
                        </a>
                    </li>
                  
                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            PM Form <small></small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>

                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                        <%--<div class="panel panel-default">--%>

                <table >
            <tr>
                <td align="left" class="style4">

                    </td>
                <td align="left" class="style4">

                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="Customer : " ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style4" colspan="4">


                <asp:DropDownList ID="drpCust" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style1">

                    </td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        Text="PM Date : " ToolTip="ชื่อลูกค้า"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  <asp:TextBox ID="txtDatePM" runat="server" CssClass="fn_Content" Width="150px"  
                         class="form-control" Font-Size="Small" ReadOnly="True"></asp:TextBox>


                </td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label6" runat="server" CssClass="fn_ContentTital" 
                        Text="To" ToolTip="ชื่อลูกค้า"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  <asp:TextBox ID="txtDatePM2" runat="server" CssClass="fn_Content" Width="150px"  
                         class="form-control" Font-Size="Small" ReadOnly="True"></asp:TextBox>


                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13" colspan="5">

                <asp:Button ID="btnSearch" runat="server" Text="Search" 
                            class="btn btn-info" />
                    &nbsp;
                 <asp:Button ID="btnExport" runat="server" Text="Export" class="btn btn-default" OnClientClick = "SetTarget();" />	   
                    
                    </td>
            </tr>       
            
          
        </table>

        <br />
        
    
          <%--<asp:GridView ID="GridView" 
                Font-Size="Medium" 
                AutoGenerateColumns="False" 
                onrowdatabound="GridView_RowDataBound"
                runat="server" Width="100%" PageSize="15">
                
       
                <Columns>
                      <asp:HyperLinkField DataNavigateUrlFields="PMID,UserID" 
                                            DataNavigateUrlFormatString="PM_WorkOrder.aspx?PMID={0}&UserID={1}" 
                                            DataTextField="PMID" HeaderText="เลขที่ใบสั่งงาน" 
                                            NavigateUrl="PM_WorkOrder.aspx?PMID={0}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>

                    
                    <asp:TemplateField HeaderText ="รหัสลูกค้า">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomer_id" runat ="server" Text ='<%# Eval("Customer_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                       <asp:TemplateField HeaderText ="ชื่อลูกค้า">
                        <ItemTemplate>
                            <asp:Label ID="lblCustName" runat ="server" Text ='<%# Eval("CustName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText ="วันที่แจ้ง PM">
                        <ItemTemplate>
                            <asp:Label ID="lblPMDate" runat ="server" Text ='<%# Eval("PMDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText ="พนักงานรับแจ้ง">
                        <ItemTemplate>
                            <asp:Label ID="lblReceptionnist_Name" runat ="server" Text ='<%# Eval("Receptionnist_Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText ="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblPM_Status" runat ="server" Text ='<%# Eval("PM_Status") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>--%>

                <asp:GridView ID="GridView" CssClass="footable" runat="server" AutoGenerateColumns="false"
                    Style="max-width: 100%">
                    <Columns>
                           <asp:HyperLinkField DataNavigateUrlFields="PMID,UserID" 
                                            DataNavigateUrlFormatString="PM_WorkOrder.aspx?PMID={0}&UserID={1}" 
                                            DataTextField="PMID" HeaderText="PM ID" 
                                            NavigateUrl="PM_WorkOrder.aspx?PMID={0}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>
                   
                        <asp:BoundField DataField="Customer_id" HeaderText="Customer ID" />
                        <asp:BoundField DataField="CustName" HeaderText="Customer Name" />
                        <asp:BoundField DataField="PMDate" HeaderText="PM Date" />
                        <asp:BoundField DataField="Receptionnist_Name" HeaderText="Receptionnist" />    
                        <asp:BoundField DataField="PM_Status" HeaderText="Status" />
                    </Columns>
                </asp:GridView>

                        <%--</div>--%>
                    </div>
                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


   
     <script>
         // SCRIPT FOR THE MOUSE EVENT.
         function MouseEvents(objRef, evt) {
             if (evt.type == "mouseover") {
                 objRef.style.cursor = 'pointer';
                 objRef.style.backgroundColor = "#EEEED1";
             }
             else {
                 if (evt.type == "mouseout") objRef.style.backgroundColor = "#FFF";
             }
         }
        </script>


     <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtDatePM.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                     $jq('#<%=txtDatePM2.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
      </script>

         <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#drpDep").select2({
                    });
                });
            </script>


        <%--Gridview--%>
        <%--    https://www.aspsnippets.com/Articles/Bootstrap-Responsive-GridView-for-Mobile-Phone-Tablet-and-Desktop-display-in-ASPNet-using-jQuery.aspx--%>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
    rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('[id*=GridView]').footable();
            });
        </script>        

</form>
</body>
</html>
