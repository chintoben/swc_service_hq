﻿Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class PM_WorkOrder50Hr
    Inherits System.Web.UI.Page
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim dtt As New DataTable
    Private sms As New PKMsg("")
    Dim sqlCheck, sql, sql1 As String
    Dim dt, dt2, dt1 As New DataTable
    Dim Cmd As OleDbCommand
    Dim Dr As OleDbDataReader
    Dim UserID, ID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ID = Request.QueryString("ID")
        UserID = Request.QueryString("UserID")
        lblUserName.Text = UserID

        If Not IsPostBack Then

            If UserID <> "" Then
                sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
                sql1 += " Where  [employee_id] = '" & UserID & "'"
                dt1 = db_HR.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    lblUserName.Text = dt1.Rows(0)("fullname")
                End If

            End If

            '  Check New  หรือ old 
            sql += " SELECT * FROM [TB_PM50Hr] "
            sql += " Where  ID = '" & ID & "'"
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count <= 0 Then
                'new
            Else
                LoadData(ID)
            End If
        End If

    End Sub

    Sub LoadData(ID)
        sql = " select * "
        sql += " from [TB_PM50Hr] "
        sql += " where [ID] = '" & ID & "' "

        dt = db_SWC.GetDataTable(sql)

        If dt.Rows.Count > 0 Then

            If dt.Rows(0)("oil1") = "Success" Then
                chk1.Checked = True
            Else
                chk1.Checked = False
            End If

            If dt.Rows(0)("filter2") = "Success" Then
                chk2.Checked = True
            Else
                chk2.Checked = False
            End If

            If dt.Rows(0)("Flushing3") = "Success" Then
                chk3.Checked = True
            Else
                chk3.Checked = False
            End If

            If dt.Rows(0)("fuel4") = "Success" Then
                chk4.Checked = True
            Else
                chk4.Checked = False
            End If

            If dt.Rows(0)("check5") = "Success" Then
                chk5.Checked = True
            Else
                chk5.Checked = False
            End If

            If dt.Rows(0)("AcceleratedTongue6") = "Success" Then
                chk6.Checked = True
            Else
                chk6.Checked = False
            End If

            If dt.Rows(0)("BUDS7") = "Success" Then
                chk7.Checked = True
            Else
                chk7.Checked = False
            End If

            If dt.Rows(0)("SparkPlug8") = "Success" Then
                chk8.Checked = True
            Else
                chk8.Checked = False
            End If

            If dt.Rows(0)("wire9") = "Success" Then
                chk9.Checked = True
            Else
                chk9.Checked = False
            End If

            If dt.Rows(0)("switchAlarm10") = "Success" Then
                chk10.Checked = True
            Else
                chk10.Checked = False
            End If

            If dt.Rows(0)("JointSteeringSystem11") = "Success" Then
                chk11.Checked = True
            Else
                chk11.Checked = False
            End If

            If dt.Rows(0)("SteeringSystem12") = "Success" Then
                chk12.Checked = True
            Else
                chk12.Checked = False
            End If

            If dt.Rows(0)("OTAS13") = "Success" Then
                chk13.Checked = True
            Else
                chk13.Checked = False
            End If

            If dt.Rows(0)("Carbon14") = "Success" Then
                chk14.Checked = True
            Else
                chk14.Checked = False
            End If

            If dt.Rows(0)("Wearing15") = "Success" Then
                chk15.Checked = True
            Else
                chk15.Checked = False
            End If

            If dt.Rows(0)("TestIBR16") = "Success" Then
                chk16.Checked = True
            Else
                chk16.Checked = False
            End If

            If dt.Rows(0)("IBR17") = "Success" Then
                chk17.Checked = True
            Else
                chk17.Checked = False
            End If

            If dt.Rows(0)("Battery18") = "Success" Then
                chk18.Checked = True
            Else
                chk18.Checked = False
            End If


            Remark1.Text = dt.Rows(0)("oilRemark1")
            Remark2.Text = dt.Rows(0)("filterRemark2")
            Remark3.Text = dt.Rows(0)("FlushingRemark3")
            Remark4.Text = dt.Rows(0)("fuelRemark4")
            Remark5.Text = dt.Rows(0)("checkRemark5")
            Remark6.Text = dt.Rows(0)("AcceleratedTongueRemark6")
            Remark7.Text = dt.Rows(0)("BUDSRemark7")
            Remark8.Text = dt.Rows(0)("SparkPlugRemark8")
            Remark9.Text = dt.Rows(0)("wireRemark9")
            Remark10.Text = dt.Rows(0)("switchAlarmRemark10")
            Remark11.Text = dt.Rows(0)("JointSteeringSystemRemark11")
            Remark12.Text = dt.Rows(0)("SteeringSystemRemark12")
            Remark13.Text = dt.Rows(0)("OTASRemark13")
            Remark14.Text = dt.Rows(0)("CarbonRemark14")
            Remark15.Text = dt.Rows(0)("WearingRemark15")
            Remark16.Text = dt.Rows(0)("TestIBRRemark16")
            Remark17.Text = dt.Rows(0)("IBRRemark17")
            Remark18.Text = dt.Rows(0)("BatteryRemark18")

        End If

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        sqlCheck += " SELECT * FROM [TB_PM50Hr] "
        sqlCheck += " Where  ID = '" & ID & "'"
        dt = db_SWC.GetDataTable(sqlCheck)
        If dt.Rows.Count <= 0 Then
            insert()
        Else
            Update()
        End If

    End Sub

    Sub insert()
        Dim db_SWC As New Connect_Service
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim oil1, filter2, Flushing3, fuel4, check5 As String
        Dim AcceleratedTongue6, BUDS7, SparkPlug8, wire9, switchAlarm10 As String
        Dim JointSteeringSystem11, SteeringSystem12, OTAS13, Carbon14, Wearing15 As String
        Dim TestIBR16, IBR17, Battery18 As String


        'Check Conditon
        If chk1.Checked = True Then
            oil1 = "Success"
        Else
            oil1 = ""
        End If

        If chk2.Checked = True Then
            filter2 = "Success"
        Else
            filter2 = ""
        End If

        If chk3.Checked = True Then
            Flushing3 = "Success"
        Else
            Flushing3 = ""
        End If

        If chk4.Checked = True Then
            fuel4 = "Success"
        Else
            fuel4 = ""
        End If

        If chk5.Checked = True Then
            check5 = "Success"
        Else
            check5 = ""
        End If

        If chk6.Checked = True Then
            AcceleratedTongue6 = "Success"
        Else
            AcceleratedTongue6 = ""
        End If

        If chk7.Checked = True Then
            BUDS7 = "Success"
        Else
            BUDS7 = ""
        End If

        If chk8.Checked = True Then
            SparkPlug8 = "Success"
        Else
            SparkPlug8 = ""
        End If

        If chk9.Checked = True Then
            wire9 = "Success"
        Else
            wire9 = ""
        End If

        If chk10.Checked = True Then
            switchAlarm10 = "Success"
        Else
            switchAlarm10 = ""
        End If

        If chk11.Checked = True Then
            JointSteeringSystem11 = "Success"
        Else
            JointSteeringSystem11 = ""
        End If

        If chk12.Checked = True Then
            SteeringSystem12 = "Success"
        Else
            SteeringSystem12 = ""
        End If

        If chk13.Checked = True Then
            OTAS13 = "Success"
        Else
            OTAS13 = ""
        End If

        If chk14.Checked = True Then
            Carbon14 = "Success"
        Else
            Carbon14 = ""
        End If

        If chk15.Checked = True Then
            Wearing15 = "Success"
        Else
            Wearing15 = ""
        End If

        If chk16.Checked = True Then
            TestIBR16 = "Success"
        Else
            TestIBR16 = ""
        End If

        If chk17.Checked = True Then
            IBR17 = "Success"
        Else
            IBR17 = ""
        End If

        If chk18.Checked = True Then
            Battery18 = "Success"
        Else
            Battery18 = ""
        End If

        Dim sql As String = "INSERT INTO [TB_PM50Hr] ( "
        sql &= " ID ,oil1	      ,oilRemark1	      ,filter2	      ,filterRemark2	      ,Flushing3	      ,FlushingRemark3	      ,fuel4	      ,fuelRemark4	      ,check5	      ,checkRemark5"
        sql &= " ,AcceleratedTongue6	      ,AcceleratedTongueRemark6	      ,BUDS7	      ,BUDSRemark7	      ,SparkPlug8	      ,SparkPlugRemark8	      ,wire9	      ,wireRemark9	      ,switchAlarm10	      ,switchAlarmRemark10"
        sql &= " ,JointSteeringSystem11	      ,JointSteeringSystemRemark11	      ,SteeringSystem12	      ,SteeringSystemRemark12	      ,OTAS13	      ,OTASRemark13	      ,Carbon14	      ,CarbonRemark14	      ,Wearing15	      ,WearingRemark15"
        sql &= " ,TestIBR16	      ,TestIBRRemark16	      ,IBR17	      ,IBRRemark17	      ,Battery18	      ,BatteryRemark18	 ,CreateBy	      ,CreateDate"
        sql &= "  )"

        sql &= " VALUES ('" & ID & "','" & oil1 & "','" & Remark1.Text & "','" & filter2 & "','" & Remark2.Text & "','" & Flushing3 & "','" & Remark3.Text & "','" & fuel4 & "','" & Remark4.Text & "','" & check5 & "','" & Remark5.Text & "' "
        sql &= " ,'" & AcceleratedTongue6 & "','" & Remark6.Text & "','" & BUDS7 & "','" & Remark7.Text & "','" & SparkPlug8 & "','" & Remark8.Text & "','" & wire9 & "','" & Remark9.Text & "','" & switchAlarm10 & "','" & Remark10.Text & "' "
        sql &= " ,'" & JointSteeringSystem11 & "','" & Remark11.Text & "','" & SteeringSystem12 & "','" & Remark12.Text & "','" & OTAS13 & "','" & Remark13.Text & "','" & Carbon14 & "','" & Remark14.Text & "','" & Wearing15 & "','" & Remark15.Text & "' "
        sql &= " ,'" & TestIBR16 & "','" & Remark16.Text & "','" & IBR17 & "','" & Remark17.Text & "','" & Battery18 & "','" & Remark18.Text & "'"
        sql &= " ,'" & UserID & "', getdate() ) "
        dtt = db_SWC.GetDataTable(sql)


    End Sub

    Sub Update()
        Dim x As Integer
        Dim db_SWC As New Connect_Service
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim oil1, filter2, Flushing3, fuel4, check5 As String
        Dim AcceleratedTongue6, BUDS7, SparkPlug8, wire9, switchAlarm10 As String
        Dim JointSteeringSystem11, SteeringSystem12, OTAS13, Carbon14, Wearing15 As String
        Dim TestIBR16, IBR17, Battery18 As String


        Dim sSql As String = "UPDATE [TB_PM50Hr] SET  "
        sSql += " oil1=@oil1 ,oilRemark1=@oilRemark1 ,filter2=@filter2 ,filterRemark2=@filterRemark2 ,Flushing3=@Flushing3 ,FlushingRemark3=@FlushingRemark3, fuel4=@fuel4 ,fuelRemark4=@fuelRemark4 ,check5=@check5 ,checkRemark5=@checkRemark5"
        sSql += " ,AcceleratedTongue6=@AcceleratedTongue6 ,AcceleratedTongueRemark6=@AcceleratedTongueRemark6 ,BUDS7=@BUDS7 ,BUDSRemark7=@BUDSRemark7 ,SparkPlug8=@SparkPlug8 ,SparkPlugRemark8=@SparkPlugRemark8 ,wire9=@wire9 ,wireRemark9=@wireRemark9 ,switchAlarm10=@switchAlarm10 ,switchAlarmRemark10=@switchAlarmRemark10 "
        sSql += " ,JointSteeringSystem11=@JointSteeringSystem11 ,JointSteeringSystemRemark11=@JointSteeringSystemRemark11 ,SteeringSystem12=@SteeringSystem12 ,SteeringSystemRemark12=@SteeringSystemRemark12 ,OTAS13=@OTAS13 ,OTASRemark13=@OTASRemark13,Carbon14=@Carbon14 ,CarbonRemark14=@CarbonRemark14 ,Wearing15=@Wearing15 ,WearingRemark15=@WearingRemark15 "
        sSql += " ,TestIBR16=@TestIBR16 ,TestIBRRemark16=@TestIBRRemark16 ,IBR17=@IBR17 ,IBRRemark17=@IBRRemark17 ,Battery18=@Battery18 ,BatteryRemark18=@BatteryRemark18"
        sSql += " ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate  "

        sSql += " Where ID ='" & ID & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            If chk1.Checked = True Then
                oil1 = "Success"
            Else
                oil1 = ""
            End If

            If chk2.Checked = True Then
                filter2 = "Success"
            Else
                filter2 = ""
            End If

            If chk3.Checked = True Then
                Flushing3 = "Success"
            Else
                Flushing3 = ""
            End If

            If chk4.Checked = True Then
                fuel4 = "Success"
            Else
                fuel4 = ""
            End If

            If chk5.Checked = True Then
                check5 = "Success"
            Else
                check5 = ""
            End If

            If chk6.Checked = True Then
                AcceleratedTongue6 = "Success"
            Else
                AcceleratedTongue6 = ""
            End If

            If chk7.Checked = True Then
                BUDS7 = "Success"
            Else
                BUDS7 = ""
            End If

            If chk8.Checked = True Then
                SparkPlug8 = "Success"
            Else
                SparkPlug8 = ""
            End If

            If chk9.Checked = True Then
                wire9 = "Success"
            Else
                wire9 = ""
            End If

            If chk10.Checked = True Then
                switchAlarm10 = "Success"
            Else
                switchAlarm10 = ""
            End If

            If chk11.Checked = True Then
                JointSteeringSystem11 = "Success"
            Else
                JointSteeringSystem11 = ""
            End If

            If chk12.Checked = True Then
                SteeringSystem12 = "Success"
            Else
                SteeringSystem12 = ""
            End If

            If chk13.Checked = True Then
                OTAS13 = "Success"
            Else
                OTAS13 = ""
            End If

            If chk14.Checked = True Then
                Carbon14 = "Success"
            Else
                Carbon14 = ""
            End If

            If chk15.Checked = True Then
                Wearing15 = "Success"
            Else
                Wearing15 = ""
            End If

            If chk16.Checked = True Then
                TestIBR16 = "Success"
            Else
                TestIBR16 = ""
            End If

            If chk17.Checked = True Then
                IBR17 = "Success"
            Else
                IBR17 = ""
            End If

            If chk18.Checked = True Then
                Battery18 = "Success"
            Else
                Battery18 = ""
            End If

            command.Parameters.Add("@oil1", Data.SqlDbType.VarChar).Value = oil1
            command.Parameters.Add("@oilRemark1", Data.SqlDbType.VarChar).Value = Remark1.Text
            command.Parameters.Add("@filter2", Data.SqlDbType.VarChar).Value = filter2
            command.Parameters.Add("@filterRemark2", Data.SqlDbType.VarChar).Value = Remark2.Text
            command.Parameters.Add("@Flushing3", Data.SqlDbType.VarChar).Value = Flushing3
            command.Parameters.Add("@FlushingRemark3", Data.SqlDbType.VarChar).Value = Remark3.Text
            command.Parameters.Add("@fuel4", Data.SqlDbType.VarChar).Value = fuel4
            command.Parameters.Add("@fuelRemark4", Data.SqlDbType.VarChar).Value = Remark4.Text
            command.Parameters.Add("@check5", Data.SqlDbType.VarChar).Value = check5
            command.Parameters.Add("@checkRemark5", Data.SqlDbType.VarChar).Value = Remark5.Text
            command.Parameters.Add("@AcceleratedTongue6", Data.SqlDbType.VarChar).Value = AcceleratedTongue6
            command.Parameters.Add("@AcceleratedTongueRemark6", Data.SqlDbType.VarChar).Value = Remark6.Text
            command.Parameters.Add("@BUDS7", Data.SqlDbType.VarChar).Value = BUDS7
            command.Parameters.Add("@BUDSRemark7", Data.SqlDbType.VarChar).Value = Remark7.Text
            command.Parameters.Add("@SparkPlug8", Data.SqlDbType.VarChar).Value = SparkPlug8
            command.Parameters.Add("@SparkPlugRemark8", Data.SqlDbType.VarChar).Value = Remark8.Text
            command.Parameters.Add("@wire9", Data.SqlDbType.VarChar).Value = wire9
            command.Parameters.Add("@wireRemark9", Data.SqlDbType.VarChar).Value = Remark9.Text
            command.Parameters.Add("@switchAlarm10", Data.SqlDbType.VarChar).Value = switchAlarm10
            command.Parameters.Add("@switchAlarmRemark10", Data.SqlDbType.VarChar).Value = Remark10.Text
            command.Parameters.Add("@JointSteeringSystem11", Data.SqlDbType.VarChar).Value = JointSteeringSystem11
            command.Parameters.Add("@JointSteeringSystemRemark11", Data.SqlDbType.VarChar).Value = Remark11.Text
            command.Parameters.Add("@SteeringSystem12", Data.SqlDbType.VarChar).Value = SteeringSystem12
            command.Parameters.Add("@SteeringSystemRemark12", Data.SqlDbType.VarChar).Value = Remark12.Text
            command.Parameters.Add("@OTAS13", Data.SqlDbType.VarChar).Value = OTAS13
            command.Parameters.Add("@OTASRemark13", Data.SqlDbType.VarChar).Value = Remark13.Text
            command.Parameters.Add("@Carbon14", Data.SqlDbType.VarChar).Value = Carbon14
            command.Parameters.Add("@CarbonRemark14", Data.SqlDbType.VarChar).Value = Remark14.Text
            command.Parameters.Add("@Wearing15", Data.SqlDbType.VarChar).Value = Wearing15
            command.Parameters.Add("@WearingRemark15", Data.SqlDbType.VarChar).Value = Remark15.Text
            command.Parameters.Add("@TestIBR16", Data.SqlDbType.VarChar).Value = TestIBR16
            command.Parameters.Add("@TestIBRRemark16", Data.SqlDbType.VarChar).Value = Remark16.Text
            command.Parameters.Add("@IBR17", Data.SqlDbType.VarChar).Value = IBR17
            command.Parameters.Add("@IBRRemark17", Data.SqlDbType.VarChar).Value = Remark17.Text
            command.Parameters.Add("@Battery18", Data.SqlDbType.VarChar).Value = Battery18
            command.Parameters.Add("@BatteryRemark18", Data.SqlDbType.VarChar).Value = Remark18.Text

            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = UserID
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        Dim sqlReport As String = " 1=1 "

        '{VW_CM.CMID}="CM17000005"
        If ID <> "" Then
            sqlReport = sqlReport & " and {VW_PM50.ID} = '" & ID & "' "
        End If


        Server.Transfer("RepairOrderReport.aspx?Form=PM50&sql=" & ID)




    End Sub

End Class