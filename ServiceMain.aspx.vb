﻿Imports System.Data.SqlClient

Public Class ServiceMain
    Inherits System.Web.UI.Page
    Dim UserID As String
    Dim sql1, sql2, sql3, sql4, sqlfullname As String
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim dt, dt1, dt2, dt3, dt4, dtfullname, dtt As New DataTable
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        UserID = Request.QueryString("UserID")
        lblUserName.Text = UserID

        If Not IsPostBack Then

            If UserID <> "" Then
                sqlfullname += " SELECT  [fullname]  FROM [TB_Employee] "
                sqlfullname += " Where  [employee_id] = '" & UserID & "'"
                dtfullname = db_HR.GetDataTable(sqlfullname)
                If dtfullname.Rows.Count > 0 Then
                    lblUserName.Text = dtfullname.Rows(0)("fullname")
                End If

            End If

            If UserID <> "" Then
                sql1 += " SELECT count([id]) as num  FROM [TB_RepiarOrder] where  [type] = 'RO' and [Stat] <> 'Cancel' "
                dt1 = db_SWC.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    lblRO.Text = dt1.Rows(0)("num")
                End If

                sql2 += " SELECT count([id]) as num  FROM [TB_RepiarOrder] where  [type] = 'PM' and [Stat] <> 'Cancel'"
                dt2 = db_SWC.GetDataTable(sql2)
                If dt2.Rows.Count > 0 Then
                    lblPM.Text = dt2.Rows(0)("num")
                End If

                sql3 += " SELECT count([id]) as num  FROM [TB_RepiarOrder] where  [type] = 'CL' and [Stat] <> 'Cancel'"
                dt3 = db_SWC.GetDataTable(sql3)
                If dt3.Rows.Count > 0 Then
                    lblCL.Text = dt3.Rows(0)("num")
                End If

                sql4 += " SELECT count([id]) as num  FROM [TB_RepiarOrder] where  [type] = 'CA' and [Stat] <> 'Cancel'"
                dt4 = db_SWC.GetDataTable(sql4)
                If dt4.Rows.Count > 0 Then
                    lblCA.Text = dt4.Rows(0)("num")
                End If

            End If


            loaddataGrid()

        End If
    End Sub

    Sub loaddataGrid()

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(db_SWC.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.4.20;initial catalog=DB_SwcService;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try

                constr = "data source=10.0.4.20;initial catalog=DB_SwcService;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "select '" & UserID & "' as UserID ,* " & vbCrLf
            sql = sql & " from [VW_RepairOrder] "
            sql = sql & " where Stat not in ('Close','Cancel') order by ID " & vbCrLf

            ' sql = sql & " order by [PMID] "

            Dim dt As New DataTable
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                ShowNoResultFound(dt, GridView1)
            End If

        Catch

        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView1.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        GridView1.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        GridView1.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView1.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub



    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub


End Class