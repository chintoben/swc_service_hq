﻿Imports System.Data.SqlClient

Public Class Authorize
    Inherits System.Web.UI.Page
    Private sms As New PKMsg("")
    Dim UserID As String
    Dim sql1, sql2, sqlRole As String
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim dt, dt1, dt2, dt3, dtt, dtRole As New DataTable
    Dim Role As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UserID = Request.QueryString("UserID")
        lblUserName.Text = UserID

        sqlRole += " SELECT  [Role]  FROM [TB_Authorize] "
        sqlRole += " Where  [Username] = '" & UserID & "'"
        dtRole = db_SWC.GetDataTable(sqlRole)
        If dtRole.Rows.Count > 0 Then
            Role = dtRole.Rows(0)("Role")
        End If


        If Not IsPostBack Then

            If UserID <> "" Then
                sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
                sql1 += " Where  [employee_id] = '" & UserID & "'"
                dt1 = db_HR.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    lblUserName.Text = dt1.Rows(0)("fullname")
                End If

                If Role <> "Admin" Then
                    sms.Msg = ""
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                    'Exit Sub
                    Server.Transfer("Service_NoLicense.aspx?UserID=" & UserID)
                End If

            End If

        End If

    End Sub

    Protected Sub btnADD_Click(sender As Object, e As EventArgs) Handles btnADD.Click
        Dim db_SWC As New Connect_Service
        Dim sqlCon As New SqlConnection
        Dim sqlCmd As New SqlCommand
        Dim sql As String = ""
        Dim TaskId As String = ""
        Dim tr As SqlTransaction
        Dim ErrorMsg As String = ""

        If txtUsername.Text = "" Or txtPassword.Text = "" Or DropRole.SelectedValue = "" Then
            sms.Msg = "กรุณาระบุข้อมูลให้ครบ!!!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.Constr2)
            sqlCon.Open()
        End If

        tr = sqlCon.BeginTransaction()
        Try
            sql = "INSERT INTO [TB_Authorize] "
            sql = sql & "([Username],[Password],[Role],[Status],[CreateDate])"
            sql = sql & " Values (@F1,@F2,@F3,@F4,@F5)"
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlCon, tr)
            cmd.Parameters.Add("@F1", SqlDbType.VarChar, 0).Value = txtUsername.Text
            cmd.Parameters.Add("@F2", SqlDbType.VarChar, 0).Value = txtPassword.Text
            cmd.Parameters.Add("@F3", SqlDbType.VarChar, 0).Value = DropRole.SelectedValue
            cmd.Parameters.Add("@F4", SqlDbType.VarChar, 0).Value = "Active"
            cmd.Parameters.Add("@F5", SqlDbType.VarChar, 0).Value = Date.Today.ToString("yyyy-MM-dd")
            cmd.ExecuteNonQuery()

            tr.Commit()
            sqlCon.Close()
            txtUsername.Text = ""
            DropRole.SelectedValue = ""
            txtPassword.Text = ""

        Catch ex As Exception

            tr.Rollback()
            sqlCon.Close()

            Exit Sub
        End Try
        GridView1.DataBind()
    End Sub

End Class