﻿Public Class RepairOrderType
    Inherits System.Web.UI.Page
    Dim UserID, ID, Role, type As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        UserID = Request.QueryString("UserID")
        lblUserName.Text = UserID

    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Server.Transfer("RepairOrder.aspx?Type=RO&UserID=" & UserID)
    End Sub

    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Server.Transfer("RepairOrder.aspx?Type=PM&UserID=" & UserID)
    End Sub

    Protected Sub LinkButton3_Click(sender As Object, e As EventArgs) Handles LinkButton3.Click
        Server.Transfer("RepairOrder.aspx?Type=CL&UserID=" & UserID)
    End Sub

    Protected Sub LinkButton4_Click(sender As Object, e As EventArgs) Handles LinkButton4.Click
        Server.Transfer("RepairOrder.aspx?Type=CA&UserID=" & UserID)
    End Sub


End Class