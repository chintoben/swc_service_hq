﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Service_NoLicense.aspx.vb" Inherits="SWC_ServiceSystem.Service_NoLicense" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       
</head>
<body>
  <form id="form1" runat="server">
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Task Main</span>
                      </a>
                    </li>    
                       <li>
                                Corrective Maintenance

                        </li>
                       <li>
                           <a  href="CM_RepairOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form (CM)</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="CM_RepairOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Repair Order Report (CM)</span>
                      </a>

                    </li>

                   <h5>  Preventive Maintenance</h5>    
        
                    <li>
                         <a  href="PM_WorkOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>PM Form</span>
                        </a>
                    </li>
                    <li>
                          <a  href="PM_WorkOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>PM Report</span>
                        </a>
                    </li>
                  
                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

       <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                          No license
                            <small>คุณไม่มีสิทธิ์เข้าใช้งาน</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                         
                                <i class="fa fa-dashboard"></i>  <a href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">Task Main</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> No license
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

             <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>

                <!--END FOOTER-->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</form>
</body>
</html>
