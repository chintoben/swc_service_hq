﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Authorize.aspx.vb" Inherits="SWC_ServiceSystem.Authorize" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Service System</title>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     <style type="text/css">
         .style1
         {
             width: 345px;
             height: 31px;
         }
         .style2
         {
             width: 247px;
         }
     </style>

</head>
<body>
     <form id="form1" runat="server">
 <div id="wrapper">

             <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                        <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>    --%>

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Authorize <small>กำหนดสิทธิ์การเข้าใช้งาน</small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>
                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                        <%--<div class="panel panel-default">--%>
    

                           <table class="style1" width="100%">
                    
        
                    <tr>
                        <td class="style2">
                            <font face="Tahoma">
                            <asp:Label ID="LabelTitle1" runat="server" Font-Bold="True" 
                                Font-Names="Tahoma" Font-Size="Small" ForeColor="Black" 
                                Font-Italic="False" Width="120px">Username </asp:Label>
                            </font>
                            </td>
                        <td class="style1" align = "left">
                            <asp:TextBox ID="txtUsername" runat="server" Width="100%" Height="30px"></asp:TextBox>
                        </td>
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style7">
                            &nbsp;</td>
                    </tr>
        
                    <tr>
                        <td class="style2">
                            <font face="Tahoma">
                            <asp:Label ID="LabelTitle4" runat="server" Font-Bold="True" 
                                Font-Names="Tahoma" Font-Size="Small" ForeColor="Black" 
                                Font-Italic="False" Width="120px">Password </asp:Label>
                            </font>
                            </td>
                        <td class="style1">
                            <asp:TextBox ID="txtPassword" runat="server" Width="100%" Height="30px"></asp:TextBox>
                            </td>
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style7">
                            &nbsp;</td>
                    </tr>
        
                    <tr>
                        <td class="style2">
                            <font face="Tahoma">
                            <asp:Label ID="LabelTitle5" runat="server" Font-Bold="True" 
                                Font-Names="Tahoma" Font-Size="Small" ForeColor="Black" 
                                Font-Italic="False" Width="120px">Role </asp:Label>
                            </font>
                            </td>
                        <td class="style1">
                                 <font face="Tahoma">
                                 <asp:DropDownList ID="DropRole" runat="server" Width="100%" Height="30px">
                                     <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                     <asp:ListItem Value="User">User</asp:ListItem>
                                     <asp:ListItem Value="SuperUser">SuperUser</asp:ListItem>
                                 </asp:DropDownList>
                                 </font>
                             </td>
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style7">
                            &nbsp;</td>
                    </tr>
        
                    <tr>
                        <td class="style2">
                            &nbsp;</td>
                        <td align = "left">
                                 <asp:Button ID="btnADD" runat="server" Text="ADD" class="btn btn-primary" 
                                     Width="100px" />
                                 </td>
                         
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style7">

                     </td>
                    </tr>  
        
                    <tr>
                        <td class="style2">
                            &nbsp;</td>
                        <td class="style1">
                                 &nbsp;</td>
                         
                        <td class="style3">
                            &nbsp;</td>
                        <td class="style4">
                            &nbsp;</td>
                        <td class="style7">

                            &nbsp;</td>
                    </tr>  
                </table>    
       
                                    
                                          
                                    
                                                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                            AutoGenerateColumns="False" 
                                DataKeyNames="id" DataSourceID="SqlDataSource1" 
                                                            Font-Bold="False" Font-Names="Tahoma" 
                                Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" CellPadding="4" 
                                BackColor="#CCCCCC" BorderColor="#999999" 
                                                            BorderStyle="Solid" AllowSorting="True" 
                                BorderWidth="3px" CellSpacing="2" ForeColor="Black">
                                                            <FooterStyle BackColor="#CCCCCC" />
                                                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                                            <RowStyle BackColor="White" />
                                                            <Columns>

                                                              <asp:BoundField DataField="id" HeaderText="id" ReadOnly="true"  
                                                                    SortExpression="id" InsertVisible="False" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="username" HeaderText="username" 
                                                                    SortExpression="username" InsertVisible="False" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="password" HeaderText="password" 
                                                                    SortExpression="password" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                               
                                                             
                                                                <asp:TemplateField HeaderText="Role" SortExpression="Role">
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList ID="Drop1" runat="server" 
                                                                            SelectedValue='<%# Bind("Role") %>'>                                                                         
                                                                               <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                                                                 <asp:ListItem Value="User">User</asp:ListItem>
                                                                                <%-- <asp:ListItem Value="SuperUser">SuperUser</asp:ListItem>--%>
                                                                               
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="Role" runat="server" Text='<%# Bind("Role") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                       
                                                               
                                                                <asp:TemplateField HeaderText="Active" SortExpression="status">
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList ID="Drop2" runat="server" 
                                                                            SelectedValue='<%# Bind("status") %>' Height="23px" Width="82px">
                                                                            <asp:ListItem Value="Active">Active</asp:ListItem>
                                                                            <asp:ListItem Value="InActive">InActive</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>

                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chk" runat="server" 
                                                                            Checked='<%# If(Eval("status")= "Active", True, False) %>'  Enabled="False" />
                                                                    </ItemTemplate>

                                                                    <HeaderStyle HorizontalAlign="Left" />

                                                                 </asp:TemplateField>  
                                                             
                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                 

                                                                     DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >

                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>
                                                                   
                                                            </Columns>
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
 
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>" 
        DeleteCommand="DELETE FROM [TB_Authorize] where [id] = @id" 
        SelectCommand="SELECT id,username, password,role, status FROM [TB_Authorize]   " 
        UpdateCommand="UPDATE [TB_Authorize] set  username=@username , password=@password, status=@status ,role=@role  where [id] = @id"
        >
        
        <UpdateParameters>
        
           <%-- <asp:Parameter Name="Resource" />--%>
            <asp:Parameter Name="Role" />
            <asp:Parameter Name="status" />
            
            <%--<asp:controlparameter name="username" controlid="lblres_id" />--%>
            <%--<asp:controlparameter name="date" controlid="lbldate" />--%>
           
        </UpdateParameters>
        
    </asp:SqlDataSource>





                      <%--  </div>--%>
                    </div>
                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


   
     <script>
         // SCRIPT FOR THE MOUSE EVENT.
         function MouseEvents(objRef, evt) {
             if (evt.type == "mouseover") {
                 objRef.style.cursor = 'pointer';
                 objRef.style.backgroundColor = "#EEEED1";
             }
             else {
                 if (evt.type == "mouseout") objRef.style.backgroundColor = "#FFF";
             }
         }
</script>


</form>
</body>

</html>