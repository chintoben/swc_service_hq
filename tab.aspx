﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tab.aspx.vb" Inherits="SWC_ServiceSystem.tab" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Create Incident</title>

   
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <script src="assets/js/chart-master/Chart.js"></script>

 <%--tab--%>
  <script src="../vendor/jquery-1.7.1.min.js" type="text/javascript"></script> 
  <script src="../vendor/jquery.hashchange.min.js" type="text/javascript"></script>
  <script src="../lib/jquery.easytabs.min.js" type="text/javascript"></script>

  <style>
    /* Example Styles for Demo */
    .etabs { margin: 0; padding: 0; }
    .tab { display: inline-block; zoom:1; *display:inline; background: #eee; border: solid 1px #999; border-bottom: none; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; }
    .tab a { font-size: 14px; line-height: 2em; display: block; padding: 0 10px; outline: none; }
    .tab a:hover { text-decoration: underline; }
    .tab.active { background: #fff; padding-top: 6px; position: relative; top: 1px; border-color: #666; }
    .tab a.active { font-weight: bold; }
    .tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
    .panel-container { margin-bottom: 10px; }
  </style>

  <script type="text/javascript">
      $(document).ready(function () {
          $('#tab-container').easytabs();
      });
  </script>

</head>
<body>
   <h2><a href="http://os.alfajango.com/easytabs">View more demos and docs</a></h2>

<div id="tab-container" class='tab-container'>
 <ul class='etabs'>
   <li class='tab'><a href="#tabs1-html">HTML Markup</a></li>
   <li class='tab'><a href="#tabs1-js">Required JS</a></li>
   <li class='tab'><a href="#tabs1-css">Example CSS</a></li>
 </ul>
 <div class='panel-container'>
  <div id="tabs1-html">
   <h2>HTML Markup for these tabs</h2>

  <pre>
    1111111111111111111
  </pre>

  </div>

   <div id="tabs1-js">
   <h2>JS for these tabs</h2>

   <pre>
22222222222222222
  </pre>

 

  </div>
  <div id="tabs1-css">
   <h2>CSS Styles for these tabs</h2>
    3333333333333

  </div>
 </div>
</div>

<%--  <div class="showback">
                        <asp:Button ID="btnCreateIncident" runat="server" Text="Create Incident" class="btn btn-primary" />
						&nbsp;<asp:Button ID="btnClose" runat="server" Text="Close Job" 
                            class="btn btn-success" />
						&nbsp;
						<button type="button" class="btn btn-default">Default</button>
						<button type="button" class="btn btn-info">Info</button>
						<button type="button" class="btn btn-warning">Warning</button>
						<button type="button" class="btn btn-danger">Danger</button>
                        
    </div>--%>

       


             </div>
              </div><! --/row -->
          </section>
      </section>



      <!--main content end-->
      <!--footer start-->
    <%-- <footer class="site-footer">
          <div class="text-center">
              MIS Development Copyright © 2017 | All rights Reserved
              <a href="index.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>--%>
      <!--footer end-->
    <%--  </section>--%>

    <!-- js placed at the end of the document so the pages load faster -->
<%--    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>--%>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	

	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        

        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
  
     <%--  <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSale").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSaleItem").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpReceptionnist").select2({
                             });
                         });
                    </script>
                    
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpStatus").select2({
                            });
                        });
                    </script>--%>
                    

</body>
</html>
