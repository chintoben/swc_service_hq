﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CM_RepairOrder.aspx.vb" Inherits="SWC_ServiceSystem.CM_RepairOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Service System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


     <%--TAB--%>
<%--     http://www.tutorialrepublic.com/faq/how-to-keep-the-current-tab-active-on-page-reload-in-bootstrap.php--%>
<%--<link rel="stylesheet" href="css/bootstrap.min.css">--%>
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
           
</head>

<body>
    <form id="form1" runat="server">
    
    <div id="wrapper">

       
            <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Task Main</span>
                      </a>
                    </li>    
                       <li>
                                Corrective Maintenance

                        </li>
                       <li>
                           <a  href="CM_RepairOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form (CM)</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="CM_RepairOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Repair Order Report (CM)</span>
                      </a>

                    </li>

                   <h5>  Preventive Maintenance</h5>    
        
                    <li>
                         <a  href="PM_WorkOrder.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>PM Form</span>
                        </a>
                    </li>
                    <li>
                          <a  href="PM_WorkOrderReport.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>PM Report</span>
                        </a>
                    </li>
                  
                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Repair Order <small>ใบสั่งซ่อม</small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>
                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">

    <%--Start TAB--%>
      <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a data-toggle="tab" href="#sectionA">REPAIR ORDER FORM</a></li>
        <li><a data-toggle="tab" href="#sectionB">PARTS REQUIRED</a></li>
        <li><a data-toggle="tab" href="#sectionC">LABOR</a></li>
    </ul>
    <div class="tab-content">
        <div id="sectionA" class="tab-pane fade in active">
           <br />
             <table width="100%" dir="ltr" >	                    
            <tr>
                <td align="right" class="style10">
                    <asp:Label ID="Label18" runat="server" 
                        Text="เลขที่ NO. :" Width="87px"  Font-Size="Small"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style8" colspan="2">

                  <asp:TextBox ID="txtCMID" runat="server" CssClass="fn_Content" Width="150px"  
                        class="form-control" Font-Size="Small" ReadOnly="True"></asp:TextBox>


                </td>
                <td align="right" class="style8">
                    <asp:Label ID="Label48" runat="server" CssClass="fn_ContentTital" 
                        Text="สถานะ :" Font-Size="Small" Width="45px"></asp:Label>
                </td>
                <td align="left" class="style8">

                <asp:DropDownList ID="drpStatus" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small">
                    <asp:ListItem>Open</asp:ListItem>
                    <asp:ListItem>Close</asp:ListItem>
                    </asp:DropDownList>


                    </td>
                <td align="left" class="style9">
                </td>
                <td align="left" class="style8">
                    </td>
            </tr>
        
          
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label63" runat="server" 
                        Text="วันที่แจ้งซ่อม :" Width="87px"  Font-Size="Small"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style5" colspan="2">

                  <asp:TextBox ID="txtDateRepair" runat="server" CssClass="fn_Content" Width="150px"  
                         class="form-control" Font-Size="Small" ReadOnly="True"></asp:TextBox>


                </td>
                <td align="right" class="style5">
                                   
                    <asp:Label ID="Label64" runat="server" CssClass="fn_ContentTital" 
                        Text="พนักงานรับแจ้ง :" Font-Size="Small" Width="90px"></asp:Label>

                </td>
                <td align="left" class="style5">

                <asp:DropDownList ID="drpReceptionnist" runat="server" Width="100%"  class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>

                    </td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="left" class="style5">
                                   
                    &nbsp;</td>
            </tr>
        
          
            <tr>
                <td align="right" class="style4">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style5" colspan="2">

                    &nbsp;</td>
                <td align="right" class="style5">
                                   
                    <asp:Label ID="Label70" runat="server" CssClass="fn_ContentTital" 
                        Text="ช่างผู้รับผิดชอบ :" Font-Size="Small" Width="90px"></asp:Label>

                </td>
                <td align="left" class="style5">

                <asp:DropDownList ID="drpTechnician" runat="server" Width="100%"  class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>

                    </td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="left" class="style5">
                    &nbsp;</td>
            </tr>
        
          
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label53" runat="server" Font-Bold="True" Font-Size="Small" 
                        ForeColor="Red" Text="*"></asp:Label>
                    <asp:Label ID="Label33" runat="server" CssClass="fn_ContentTital" 
                        Text="ลูกค้า :" Font-Size="Small"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style5" colspan="2">

                <asp:DropDownList ID="drpCust" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" AutoPostBack="True">
                    </asp:DropDownList>

                 

                <%-- <div class="form-group">
                    <label for="gender1" class="col-sm-2 control-label">With Bootstrap:</label>
                    <div class="col-sm-2">
                    <select class="form-control" id="gender1">
                      <option>Male</option>
                      <option>Female</option>
                    </select>          
          
                        </div>
                </div>          --%>


                </td>
                <td align="right" class="style5">
                                   
                    <asp:Label ID="Label62" runat="server" CssClass="fn_ContentTital" 
                        Text="ชื่อลูกค้า :" Height="21px" Font-Size="Small"></asp:Label>
                                   

                </td>
                <td align="left" class="style5">
                                   
                    <asp:Label ID="lblCustName" runat="server" Text="-" Width="100%"></asp:Label>

                    </td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="left" class="style5">
                    &nbsp;</td>
            </tr>
        
          
            <tr>
                <td align="right" class="style4">
                    <asp:Label ID="Label60" runat="server" CssClass="fn_ContentTital" 
                        Text="ที่อยู่ :" Width="99px" Height="21px" Font-Size="Small"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style5" colspan="2">

                    <asp:Label ID="lblCustAdd" runat="server" Text="-" Width="100%"></asp:Label>


                </td>
                <td align="right" class="style5">
                                   
                    <asp:Label ID="Label34" runat="server" CssClass="fn_ContentTital" 
                        Text="เบอร์โทรศัพท์ :" Width="90px" Font-Size="Small"></asp:Label>
                                   

                </td>
                <td align="left" class="style5">
                                   
                    <asp:Label ID="lblCustTel" runat="server" Text="-" Width="100%"></asp:Label>

                    </td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="left" class="style5">
                    &nbsp;</td>
            </tr>
        
            <tr>
                <td align="right" class="style3">
                    &nbsp;</td>
                
                 <td align ="left" class="style2" >

                     &nbsp;</td>
                
                 <td align ="right" >

                     &nbsp;</td>
                
                                 <td align="left" class="style7" colspan="4">
                                   
                                     &nbsp;</td>

            </tr>        
            <tr>
                <td align="right" class="style3">
                    <asp:Label ID="Label58" runat="server" Font-Bold="True" Font-Size="Small" 
                        ForeColor="Red" Text="*"></asp:Label>
                    <asp:Label ID="Label59" runat="server" CssClass="fn_ContentTital" 
                        Text="Item :" Font-Size="Small"></asp:Label>
                </td>
                
                 <td align ="left" class="style2" colspan="2" >

                    <asp:DropDownList ID="drpItem" runat="server" AutoPostBack="True"   
                         ClientIDMode="Static" class="form-control"
                        Width="100%">
                      
                    </asp:DropDownList>
                </td>
                
                                 <td align="left" class="style7" colspan="4">
                                   
                                     &nbsp;</td>
            </tr>         
            <tr>
                <td align="right" class="style3">
                    <asp:Label ID="Label66" runat="server" Font-Bold="True" Font-Size="Small" 
                        ForeColor="Red" Text="*"></asp:Label>
                    &nbsp;<asp:Label ID="Label65" runat="server" CssClass="fn_ContentTital" 
                        Text="Serial Number :" Font-Size="Small"></asp:Label>
                </td>                
                 <td align ="left" class="style2" colspan="2" >
                    <asp:DropDownList ID="drpSerialNumber" runat="server"   ClientIDMode="Static" class="form-control"
                        Width="100%" AutoPostBack="True">                     
                    </asp:DropDownList>
                </td>                
                                 <td align="left" class="style7" colspan="4">
                                   
                                     &nbsp;</td>
            </tr>   
            <tr>
                <td align="right" class="style3">
                    <asp:Label ID="Label81" runat="server" Font-Bold="True" Font-Size="Small" 
                        ForeColor="Red" Text="*"></asp:Label>
                    &nbsp;<asp:Label ID="Label80" runat="server" CssClass="fn_ContentTital" 
                        Text="เลขตัวถัง :" Font-Size="Small"></asp:Label>
                </td>                
                 <td align ="left" class="style2" colspan="2" >
                    <asp:DropDownList ID="drpClassisNumber" runat="server"   ClientIDMode="Static" class="form-control"
                        Width="100%">                     
                    </asp:DropDownList>
                </td>                
                                 <td align="left" class="style7" colspan="4">
                                   
                                     &nbsp;</td>
            </tr>   
            <tr>
                <td align="right" class="style3">
                    &nbsp;</td>
                <td align="left" valign="middle" colspan="6">
                    &nbsp;</td>
            </tr>      
    </table>


    <table >
            <tr>
                <td align="left" class="style13">

                <h4>แจ้งซ่อม</h4>
                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="right" class="style12">
                    <asp:Label ID="Label13" runat="server" CssClass="fn_ContentTital" 
                        Text="รายการแจ้งซ่อม : " ToolTip="รายการแจ้งซ่อม"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style5">


                    <asp:TextBox ID="txtRepairDesc" runat="server" Width="100%"></asp:TextBox>


                    </td>
                <td align="right" valign="middle" class="style3">

                    <asp:Label ID="Label14" runat="server" CssClass="fn_ContentTital" 
                        Text="สาเหตุของอาการ : " ToolTip="รายการแจ้งซ่อม" Width="110px"></asp:Label>
                    </td>
                <td align="left" class="style6" colspan="3">

                    <asp:TextBox ID="txtRepairCause" runat="server" Width="100%"></asp:TextBox>
                    </td>
                <td align="left" class="style1">
                    &nbsp;&nbsp;

                <asp:Button ID="btnADD" runat="server" Text="ADD" class="btn btn-primary" />

                    </td>
            </tr>       
            <tr>
                <td align="right" class="style12">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style5">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style3">
                    &nbsp;</td>
                <td align="left" class="style6">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">
                    &nbsp;</td>
                <td align="left" valign="middle" class="style1">
                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>      
        </table>

         

            <asp:GridView ID="GridView1" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="RepairID" DataSourceID="SqlDataSource1" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" 
            CellPadding="3" BackColor="White" BorderColor="#999999" 
                                                           
                                                            GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid" 
         ForeColor="Black" Height="50px">
                                                         
                                                            <FooterStyle BackColor="#CCCCCC" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                
                                                            <Columns>
                                                                  <asp:BoundField DataField="RepairID" HeaderText="No." 
                                                                    SortExpression="RepairID" ReadOnly="True"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="Repair_Desc" HeaderText="รายการแจ้งซ่อม" 
                                                                    SortExpression="Repair_Desc"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="Repair_Cause" HeaderText="สาเหตุของอาการ" 
                                                                    SortExpression="Repair_Cause"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>

                                                            </Columns>
                                                              <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>" 
        DeleteCommand="DELETE FROM  TB_CMRepair where RepairID = @RepairID" 
       SelectCommand="SELECT RepairID	,Repair_Desc  ,Repair_Cause from TB_CMRepair where CMID = @CMID "
        UpdateCommand="UPDATE [TB_CMRepair] set  Repair_Desc=@Repair_Desc, Repair_Cause=@Repair_Cause , ModifyDate = getdate()  where [RepairID] = @RepairID "
        >
        
          <selectparameters>
              <asp:controlparameter name="CMID" controlid="txtCMID" />
             
          </selectparameters>

        <UpdateParameters>      
            <asp:Parameter Name="RepairID" />  
            <asp:Parameter Name="Repair_Desc" />  
            <asp:Parameter Name="Repair_Cause" />     
        </UpdateParameters>

           <DeleteParameters>
                <asp:Parameter Name="RepairID" /> 
            </DeleteParameters>
        
    </asp:SqlDataSource>
   

        </div>


        <div id="sectionB" class="tab-pane fade">
           <%--<br />--%>
                     <table >
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13">

                <%--<h4></h4>--%>
                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="right" class="style12">
                    &nbsp;</td>
                <td align="right" class="style12">
                    <asp:Label ID="Label3" runat="server" CssClass="fn_ContentTital" Width="80px"
                        Text="Part No.  : " ToolTip="รหัสค่าแรง"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style5">


                <asp:DropDownList ID="drpPartNo" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
                <td align="left" valign="middle" class="style3">

                    <asp:Label ID="lblDescriptionPart" runat="server" CssClass="fn_ContentTital" 
                        Text="Description" ToolTip="รายละเอียด" Width="100%"></asp:Label>
                    </td>
                <td align="right" class="style6">

                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        Text="Qty : " ToolTip="ค่าแรง" Width="47px"></asp:Label>

                    </td>
                <td>
                    &nbsp;</td>
                <td align="left" class="style1">


                    <asp:TextBox ID="txtQty" runat="server" Width="100%" Enabled="False"></asp:TextBox>


                    </td>
                <td align="right" class="style1">

                    <asp:Label ID="Label69" runat="server" CssClass="fn_ContentTital" 
                        Text="Price : " ToolTip="ราคา" Width="47px"></asp:Label>

                    </td>
                <td align="left" class="style1">


                    <asp:TextBox ID="txtUnitPrice" runat="server" Width="100%" Enabled="False"></asp:TextBox>


                    </td>
                <td align="right" class="style1">

                    <asp:Label ID="Label6" runat="server" CssClass="fn_ContentTital" 
                        Text="QTY Use: " ToolTip="ค่าแรง" Width="103px"></asp:Label>

                    </td>
                <td align="left" class="style1">


                    <asp:TextBox ID="txtQtyUse" runat="server" Width="100%" AutoPostBack="True">1</asp:TextBox>


                    </td>
                <td>


                    &nbsp;</td>
                <td>

                    &nbsp;</td>
                <td class="style2">


                    &nbsp;</td>
                <td align="left" class="style1">

                &nbsp;

                <asp:Button ID="btnAddPart" runat="server" Text="ADD" class="btn btn-primary" autopostback="true" />

                    </td>
            </tr>       
          
        </table>

          <asp:GridView ID="GridView3" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="PartID" DataSourceID="SqlDataSource3" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" 
            CellPadding="3" BackColor="White" BorderColor="#999999" 
                                                           
                                                            GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid" 
         ForeColor="Black" Height="50px">
                                                         
                                                            <FooterStyle BackColor="#CCCCCC" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                
                                                            <Columns>
                                                                  <asp:BoundField DataField="PartID" HeaderText="ID" 
                                                                    SortExpression="PartID" ReadOnly="True"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                 <asp:BoundField DataField="PartNo" HeaderText="Part No." 
                                                                    SortExpression="PartNo"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="Description" HeaderText="Description" 
                                                                    SortExpression="Description"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="QtyUse" HeaderText="Qty" 
                                                                    SortExpression="QtyUse"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                   <asp:BoundField DataField="Price" HeaderText="Price" 
                                                                    SortExpression="Price"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                 <asp:BoundField DataField="Amount" HeaderText="Amount" 
                                                                    SortExpression="Amount"  ReadOnly="True"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>

                                                            </Columns>
                                                              <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
          <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>" 
        DeleteCommand="DELETE FROM  TB_CMPartRequired where PartID = @PartID" 
       SelectCommand="SELECT PartID, PartNo	,Description  ,QtyUse , Price , QtyUse*Price as  Amount from TB_CMPartRequired where CMID = @CMID "
        UpdateCommand="UPDATE [TB_CMPartRequired] set  PartNo=@PartNo , Description=@Description, QtyUse=@QtyUse , Price=@Price , ModifyDate = getdate()  where [PartID] = @PartID "
        >
        
          <selectparameters>
              <asp:controlparameter name="CMID" controlid="txtCMID" />
             
          </selectparameters>

        <UpdateParameters>      
            <asp:Parameter Name="PartID" />  
            <asp:Parameter Name="PartNo" />  
            <asp:Parameter Name="Description" />     
            <asp:Parameter Name="QtyUse" />     
            <asp:Parameter Name="Price" />     
        </UpdateParameters>

           <DeleteParameters>
                <asp:Parameter Name="PartID" /> 
            </DeleteParameters>
        
    </asp:SqlDataSource>

        </div>



        <div id="sectionC" class="tab-pane fade">
        <%--<br />--%>
         <table >
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13">

                <%--<h4></h4>--%>
                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="right" class="style12">
                    &nbsp;</td>
                <td align="right" class="style12">
                    <asp:Label ID="Label1" runat="server" CssClass="fn_ContentTital" width="100px"
                        Text="Labor Code : " ToolTip="รหัสค่าแรง"></asp:Label>
                    </td>
                <td align="left" valign="middle" class="style5">


                <asp:DropDownList ID="txtLaborCode" runat="server" Width="100%" class="form-control"
                        ClientIDMode="Static" Font-Size="Small" AutoPostBack="True">
                    </asp:DropDownList>


                    </td>
                <td align="right" valign="middle" class="style3">

                    <asp:Label ID="Label2" runat="server" CssClass="fn_ContentTital" 
                        Text="Description : " ToolTip="รายละเอียด" Width="84px"></asp:Label>
                    </td>
                <td align="left" class="style6" colspan="3">

                    <asp:TextBox ID="txtDescription" runat="server" Width="100%" Enabled="False"></asp:TextBox>
                    </td>
                <td align="right" class="style1">

                    <asp:Label ID="Label67" runat="server" CssClass="fn_ContentTital" 
                        Text="Labor Rate : " ToolTip="ค่าแรง" Width="82px"></asp:Label>

                    </td>
                <td>


                    <asp:TextBox ID="txtLaborRate" runat="server" Width="100%" Enabled="False"></asp:TextBox>


                </td>
                <td>

                    <asp:Label ID="Label68" runat="server" CssClass="fn_ContentTital" 
                        Text="Working Hours : " ToolTip="ค่าแรง" Width="103px"></asp:Label>

                    </td>
                <td>


                    <asp:TextBox ID="txtHours" runat="server" Width="100%"  Text="1" ></asp:TextBox>


                    </td>
                <td align="left" class="style1">

                &nbsp;

                <asp:Button ID="btnAddLabor" runat="server" Text="ADD" class="btn btn-primary" />

                    </td>
            </tr>       
          
        </table>

        <asp:GridView ID="GridView2" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="LaborID" DataSourceID="SqlDataSource2" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" ShowFooter="True" 
                                                            Width="100%" 
            CellPadding="3" BackColor="White" BorderColor="#999999" 
                                                           
                                                            GridLines="Vertical" BorderWidth="1px" BorderStyle="Solid" 
         ForeColor="Black" Height="50px">
                                                         
                                                            <FooterStyle BackColor="#CCCCCC" />
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="Black" Font-Bold="true" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                
                                                            <Columns>
                                                                  <asp:BoundField DataField="LaborID" HeaderText="No." 
                                                                    SortExpression="LaborID" ReadOnly="True"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="LaborCode" HeaderText="Labor Code" 
                                                                    SortExpression="LaborCode"  >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DescriptionL" HeaderText="Description" 
                                                                    SortExpression="DescriptionL"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                 <asp:BoundField DataField="LaborRate" HeaderText="Labor Rate" 
                                                                    SortExpression="LaborRate"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                 <asp:BoundField DataField="WorkingHours" HeaderText="Working Hours" 
                                                                    SortExpression="WorkingHours"   >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>


                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>

                                                            </Columns>
                                                              <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                                        </asp:GridView>
          <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>" 
        DeleteCommand="DELETE FROM  TB_CMLabor where LaborID = @LaborID" 
       SelectCommand="SELECT LaborID	,LaborCode  ,DescriptionL , LaborRate , WorkingHours from TB_CMLabor where CMID = @CMID "
        UpdateCommand="UPDATE [TB_CMLabor] set  LaborCode=@LaborCode, DescriptionL=@DescriptionL , LaborRate=@LaborRate , WorkingHours=@WorkingHours , ModifyDate = getdate()  where [LaborID] = @LaborID "
        >
        
          <selectparameters>
              <asp:controlparameter name="CMID" controlid="txtCMID" />
             
          </selectparameters>

        <UpdateParameters>      
            <asp:Parameter Name="LaborCode" />  
            <asp:Parameter Name="DescriptionL" />  
            <asp:Parameter Name="LaborRate" />   
            <asp:Parameter Name="WorkingHours" />    
        </UpdateParameters>

           <DeleteParameters>
                <asp:Parameter Name="LaborID" /> 
            </DeleteParameters>
        
    </asp:SqlDataSource>
        </div>
    </div>
    
    <hr />
    <%--End TAB--%>

     <br />
                <div class="showback">
                        <asp:Button ID="btnCreateIncident" runat="server" Text="Create Repair Order" class="btn btn-primary" />
						&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" 
                            class="btn btn-info" />
                        &nbsp;
						<asp:Button ID="btnDelete" runat="server" Text="Delete" 
                            class="btn btn-warning" Visible="False" />
                        	
                        &nbsp;	
                        <asp:Button ID="btnReport" runat="server" Text="Report" class="btn btn-default" 
                            Visible="False" />	
                         		
                        &nbsp;		
                        <%--<button type="button" class="btn btn-warning">Warning</button>--%>
			                                
                </div>


           

                <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- jQuery -->
<%--    <script src="js/jquery.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
<%--    <script src="js/bootstrap.min.js"></script>--%>

    <!-- Morris Charts JavaScript -->
 <%--   <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>--%>


       <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtDateRepair.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
      </script>

    <%--        dropdown--%><%--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>--%>
                    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                    
                   
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpItem").select2({
                             });
                         });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpSerialNumber").select2({
                             });
                         });
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpClassisNumber").select2({
                            });
                        });
                    </script>
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpReceptionnist").select2({
                             });
                         });
                    </script>
                    
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#drpStatus").select2({
                            });
                        });
                    </script>

    </form>
</body>
</html>
