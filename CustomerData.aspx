﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerData.aspx.vb" Inherits="SWC_ServiceSystem.CustomerData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title>Service System</title>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style type="text/css">
        .style1
        {
            height: 39px;
        }
        .fn_ContentTital
        {}
        .style2
        {
            height: 34px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
      <div id="wrapper">

             <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>    --%>

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                           Customer <small></small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>

                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                        <%--   <div class="panel panel-default">--%>

                <table >
            <tr>
                <td align="left" class="style2">

                    &nbsp;</td>
                <td align="left" class="style2">

                    <asp:Label ID="Label9" runat="server" CssClass="fn_ContentTital" 
                        Text="Customer ID" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style2" colspan="4">


                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style2">

                    &nbsp;</td>
                <td align="left" class="style2">

                    <asp:Label ID="Label10" runat="server" CssClass="fn_ContentTital" 
                        Text="Title" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style2" colspan="4">


                  <asp:TextBox ID="txtSeries0" runat="server" CssClass="fn_Content" Width="100%"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style2">

                    </td>
                <td align="left" class="style2">

                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="First Name" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style2" colspan="4">


                  <asp:TextBox ID="txtSeries1" runat="server" CssClass="fn_Content" Width="100%"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4">

                    <asp:Label ID="Label7" runat="server" CssClass="fn_ContentTital" 
                        Text="LastName" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style4" colspan="4">


                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4" colspan="4">


                    &nbsp;</td>
            </tr>       
            <tr>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4" colspan="4">


                    &nbsp;</td>
            </tr>       
           
          
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13" colspan="5">

                <asp:Button ID="btnSearch" runat="server" Text="Search" 
                            class="btn btn-info" />
                    &nbsp;
                 <asp:Button ID="btnExport" runat="server" Text="Export" class="btn btn-default" />	   
                    
                    </td>
            </tr>       
            
          
        </table>

        <br />
        
    
         


                       <%-- <asp:BoundField DataField="serviceType" HeaderText="serviceType" />--%>
                    </div>
                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


   
     <script>
         // SCRIPT FOR THE MOUSE EVENT.
         function MouseEvents(objRef, evt) {
             if (evt.type == "mouseover") {
                 objRef.style.cursor = 'pointer';
                 objRef.style.backgroundColor = "#EEEED1";
             }
             else {
                 if (evt.type == "mouseout") objRef.style.backgroundColor = "#FFF";
             }
         }
        </script>


     <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtDateRepair.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );

                     $jq('#<%=txtDateRepair2.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
      </script>

                 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
                                  
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>

        <%--     <asp:BoundField DataField="ID" HeaderText="ID" />--%><%-- <asp:BoundField DataField="serviceType" HeaderText="serviceType" />--%>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
    rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('[id*=GridView]').footable();
            });
        </script>    
    </form>
</body>
</html>
