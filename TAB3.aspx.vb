﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class TAB3
    Inherits System.Web.UI.Page
    Dim db_SWC As New Connect_Service
    Dim db_HR As New Connect_HR
    Dim sql1, sql As String
    Dim dt, dt1, dt2, dt3, dtt As New DataTable
    Dim UserID, CMID As String
    Private sms As New PKMsg("")

    'http://www.tutorialrepublic.com/codelab.php?topic=faq&file=bootstrap-keep-last-selected-tab-active-on-page-refresh

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        UserID = "1159067" 'Request.QueryString("UserID")
        CMID = "CM17000005" 'Request.QueryString("CMID")


        If Not IsPostBack Then
            txtDateRepair.Text = Date.Now.ToString("dd/MM/yyyy")

            If UserID <> "" Then
                sql1 += " SELECT  [fullname]  FROM [TB_Employee] "
                sql1 += " Where  [employee_id] = '" & UserID & "'"
                dt1 = db_HR.GetDataTable(sql1)
                If dt1.Rows.Count > 0 Then
                    '  lblUserName.Text = dt1.Rows(0)("fullname")
                End If

            End If

            'Check New  หรือ old 
            sql += " SELECT * FROM [TB_CorrectiveMaintenance] "
            sql += " Where  CMID = '" & CMID & "'"
            dt = db_SWC.GetDataTable(sql)
            If dt.Rows.Count <= 0 Then
                'new

                GenID()
                ' RefreshGridView()

            Else
                txtCMID.Text = CMID
                txtCMID.Enabled = False
                btnCreateIncident.Visible = False

                loadCM(CMID)

            End If

        End If


        'If Me.IsPostBack Then
        '    TabName.Value = Request.Form(TabName.UniqueID)
        'End If

    End Sub

    Sub GenID()

        If txtCMID.Enabled = True Then
            ' New ID
            Dim conn As New Connect_Service
            Dim sql As String = "Select * from [TB_CorrectiveMaintenance] where len(CMID) = 10 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([CMID],3,2)"
            Dim sql2 As String = "Select Max(Right(CMID,6)) as CMID from [TB_CorrectiveMaintenance] where len(CMID) = 10 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring([CMID],3,2)"

            dt = db_SWC.GetDataTable(sql)
            dt2 = db_SWC.GetDataTable(sql2)

            If dt.Rows.Count <= 0 Then
                txtCMID.Text = "CM" & Now.ToString("yy") & "000001"
            Else
                Dim newID As Integer = CInt(dt2.Rows(0)("CMID"))
                newID += 1
                txtCMID.Text = "CM" & Now.ToString("yy") & newID.ToString("000000")
            End If

        Else


        End If



    End Sub

    'Protected Sub RefreshGridView()
    '    Dim dt As DataTable = New DataTable()
    '    Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("ServiceConnectionString").ConnectionString)
    '        Dim ad As SqlDataAdapter = New SqlDataAdapter("SELECT [ItemId],[ItemDesc],[Qty] from [VW_PartsWH] ", conn) ' where SaleId = '" & drpSale.SelectedValue & "'", conn)
    '        ad.Fill(dt)
    '        ViewState("dtSupplier") = dt
    '    End Using

    '    GVSparePart.DataSource = dt
    '    GVSparePart.DataBind()
    'End Sub

    Protected Sub drpCust_Init(sender As Object, e As EventArgs) Handles drpCust.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [CustomerId] as id,[CustName] from [VW_CustomerSale] group by   CustomerId , CustName order by CustomerId "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpCust.Items.Clear()
        drpCust.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpCust.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(1), Rs.GetString(0)))
            'drpDepartment.Items.Add(New ListItem(Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpCust_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpCust.SelectedIndexChanged

        sql1 += " SELECT  [CustAdd]  ,[CustPhone],CustName  FROM [VW_CustomerSale] "
        sql1 += " Where  [CustomerId] = '" & drpCust.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblCustAdd.Text = dt1.Rows(0)("CustAdd")
            lblCustTel.Text = dt1.Rows(0)("CustPhone")
            lblCustName.Text = dt1.Rows(0)("CustName")
        End If

        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [SaleId] as id,[SaleNum]  from [VW_CustomerSale] where  SaleId is not null and [CustomerId] like '" & drpCust.SelectedValue & "' group by [SaleId] ,[SaleNum]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSale.Items.Clear()
        drpSale.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSale.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub


    'Protected Sub cmdDlete_Click(sender As Object, e As EventArgs) Handles cmdDlete.Click

    '    ' PerformDelete()
    '    Dim str As String = String.Empty
    '    Dim strname As String = String.Empty
    '    For Each gvrow As GridViewRow In GVSparePart.Rows
    '        Dim chk As CheckBox = DirectCast(gvrow.FindControl("chk"), CheckBox)
    '        If chk IsNot Nothing And chk.Checked Then
    '            str += GVSparePart.DataKeys(gvrow.RowIndex).Value.ToString() + ","c
    '            strname += gvrow.Cells(2).Text & ","c
    '        End If
    '    Next

    '    str = str.Trim(",".ToCharArray())
    '    strname = strname.Trim(",".ToCharArray())
    '    lblmsg.Text = "Selected UserIds: <b>" & str & "</b><br/>" & "Selected UserNames: <b>" & strname & "</b>"

    'End Sub

    Public Function PerformDelete(GV As GridView, sTableName As String) As Boolean
        Dim bSaved As Boolean = False
        Dim sClause As String = "''"
        Dim sSQL As String = ""
        Dim comm As SqlCommand

        Dim sConstr As String = ConfigurationManager.ConnectionStrings("ServiceConnectionString").ConnectionString

        For Each oItem As GridViewRow In GV.Rows
            If CType(oItem.FindControl("chk"), CheckBox).Checked Then
                sClause += "," + GV.DataKeys(oItem.DataItemIndex).Value.ToString()
            End If
        Next

        sSQL = "DELETE FROM " + sTableName + " WHERE " + GV.DataKeyNames.GetValue(0) + " IN(" + sClause + ")"
        Using conn As SqlConnection = New SqlConnection(sConstr)
            conn.Open()
            comm = New SqlCommand(sSQL, conn)
            Using comm
                comm.CommandTimeout = 0
                comm.ExecuteNonQuery()
                bSaved = True
            End Using
        End Using
        Return bSaved
    End Function

    Protected Sub drpReceptionnist_Init(sender As Object, e As EventArgs) Handles drpReceptionnist.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee]   "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpReceptionnist.Items.Clear()
        drpReceptionnist.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpReceptionnist.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpReceptionnist_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpReceptionnist.SelectedIndexChanged
        sql1 += " SELECT  fullname  FROM [Tb_Employee] "
        sql1 += " Where  [employee_id] = '" & drpReceptionnist.SelectedValue & "'"
        dt1 = db_HR.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblReceptionnistName.Text = dt1.Rows(0)("fullname")
        End If
    End Sub

    Protected Sub drpSale_Init(sender As Object, e As EventArgs) Handles drpSale.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [SaleId] as id,[SaleNum]  from [VW_CustomerSale] where  SaleId is not null and [CustomerId] like '" & drpCust.SelectedValue & "' group by [SaleId] ,[SaleNum]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSale.Items.Clear()
        drpSale.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSale.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpSale_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSale.SelectedIndexChanged

        '----------
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
        sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  SaleId is not null and [SaleId] like '" & drpSale.SelectedValue & "' group by [ItemId] ,[ItemDesc]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSaleItem.Items.Clear()
        drpSaleItem.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSaleItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While

    End Sub

    Protected Sub drpSaleItem_Init(sender As Object, e As EventArgs) Handles drpSaleItem.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from VW_CustomerSale where SaleId = '" & drpSale.SelectedValue & "'", conn)
        sql = "select [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  SaleId is not null and [SaleId] like '" & drpSale.SelectedValue & "' group by [ItemId] ,[ItemDesc]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpSaleItem.Items.Clear()
        drpSaleItem.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpSaleItem.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Sub loadCM(ByVal CMID As String)
        sql = " SELECT CMID	 ,Customer_id   ,CustName	      ,CustAdd	      ,CustPhone	      ,Sale_ID	      ,Sale_ItemID	      ,Receptionnist "
        sql += " ,RepairDate ,Technician    ,Inspector	     ,Consignee	      ,CM_Status	      ,CreateBy	      ,CreateDate	      ,ModifyBy	      ,ModifyDate"
        sql += " from [VW_CM] "
        sql += " where CMID = '" & CMID & "'"

        dt = db_SWC.GetDataTable(sql)

        If dt.Rows.Count > 0 Then
            txtCMID.Text = dt.Rows(0)("CMID")
            txtDateRepair.Text = dt.Rows(0)("RepairDate")
            drpStatus.SelectedValue = dt.Rows(0)("CM_Status")
            drpReceptionnist.SelectedValue = dt.Rows(0)("Receptionnist")
            drpCust.SelectedValue = dt.Rows(0)("Customer_id")
            lblCustName.Text = dt.Rows(0)("CustName")
            lblCustAdd.Text = dt.Rows(0)("CustAdd")
            lblCustTel.Text = dt.Rows(0)("CustPhone")


            Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
            Dim sqlCmd As New SqlCommand
            Dim Rs As SqlDataReader
            Dim sql As String

            sql = "select [SaleId] as id,[SaleNum]  from [VW_CustomerSale] where  SaleId is not null group by [SaleId] ,[SaleNum]"

            If sqlCon.State = ConnectionState.Closed Then
                sqlCon = New SqlConnection(db_SWC.sqlCon)
                sqlCon.Open()
            End If
            With sqlCmd
                .Connection = sqlCon
                .CommandType = CommandType.Text
                .CommandText = sql
                Rs = .ExecuteReader
            End With
            drpSale.Items.Clear()
            drpSale.Items.Add(New ListItem("", 1))
            While Rs.Read
                drpSale.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
            End While

            drpSale.SelectedValue = dt.Rows(0)("Sale_ID")


            Dim sqlCon2 As New SqlConnection(db_SWC.sqlCon2)
            Dim sqlCmd2 As New SqlCommand
            Dim Rs2 As SqlDataReader
            Dim sql2 As String

            sql2 = "select  [ItemId] as id,[ItemDesc]  from [VW_CustomerSale] where  ItemId is not null  "

            If sqlCon2.State = ConnectionState.Closed Then
                sqlCon2 = New SqlConnection(db_SWC.sqlCon)
                sqlCon2.Open()
            End If
            With sqlCmd2
                .Connection = sqlCon2
                .CommandType = CommandType.Text
                .CommandText = sql2
                Rs2 = .ExecuteReader
            End With
            drpSaleItem.Items.Clear()
            drpSaleItem.Items.Add(New ListItem("", 1))
            While Rs2.Read
                drpSaleItem.Items.Add(New ListItem(Rs2.GetString(0) + " : " + Rs2.GetString(1), Rs2.GetString(0)))
            End While

            drpSaleItem.SelectedValue = dt.Rows(0)("Sale_ItemID")



        End If

    End Sub

    Protected Sub btnADD_Click(sender As Object, e As EventArgs) Handles btnADD.Click

        'insert()
        'insert to CM and Repair

        If txtCMID.Enabled = True Then
            GenID()

            'Repair
            Dim sql0 As String = "INSERT INTO [TB_CMRepair] ([CMID]  ,[Repair_Desc] ,[Repair_Cause], CreateBy, CreateDate)"
            sql0 &= "VALUES ('" & txtCMID.Text & "','" & txtRepairDesc.Text & "','" & txtRepairCause.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql0)
            GridView1.DataBind()

            'Main
            Dim sql As String = "INSERT INTO [TB_CorrectiveMaintenance] (CMID, Customer_id , CreateBy, CreateDate"
            sql &= ")"
            sql &= "VALUES ('" & txtCMID.Text & "','" & drpCust.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            txtCMID.Enabled = False
        Else
            Dim sql As String = "INSERT INTO [TB_CMRepair] ([CMID]  ,[Repair_Desc] ,[Repair_Cause], CreateBy, CreateDate)"
            sql &= "VALUES ('" & txtCMID.Text & "','" & txtRepairDesc.Text & "','" & txtRepairCause.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            GridView1.DataBind()

        End If


    End Sub

    Protected Sub btnCreateIncident_Click(sender As Object, e As EventArgs) Handles btnCreateIncident.Click

        If txtCMID.Enabled = True Then
            GenID()

            Dim sql As String = "INSERT INTO [TB_CorrectiveMaintenance] (CMID,Customer_id ,Sale_ID ,Sale_ItemID  "
            sql &= ",Receptionnist ,RepairDate,CM_Status, CreateBy, CreateDate"
            ' sql &= ",Technician ,Inspector ,Consignee"
            sql &= ")"
            sql &= "VALUES ('" & txtCMID.Text & "','" & drpCust.SelectedValue & "','" & drpSale.SelectedValue & "','" & drpSaleItem.SelectedValue & "'"
            sql &= ",'" & drpReceptionnist.SelectedValue & "','" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "' "
            sql &= ",'" & drpStatus.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            txtCMID.Enabled = False
            btnCreateIncident.Visible = False
        Else
            'Update
            UpdateCM()
        End If


    End Sub

    Sub UpdateCM()
        Dim Cn As New SqlConnection(db_SWC.sqlCon)
        Dim sqlcon As New OleDbConnection(db_SWC.cnPAStr)
        Dim sqlcom As New OleDbCommand

        Dim sSql As String = "UPDATE [TB_CorrectiveMaintenance] SET  "
        sSql += " [Customer_id]='" & drpCust.SelectedValue & "',"
        sSql += " [Sale_ID]='" & drpSale.SelectedValue & "',"
        sSql += " [Sale_ItemID]='" & drpSaleItem.SelectedValue & "',"
        sSql += " [Receptionnist]='" & drpReceptionnist.SelectedValue & "',"
        sSql += " [RepairDate]='" & DateTime.Parse(txtDateRepair.Text).ToString("yyyy-MM-dd") & "',"
        sSql += " [CM_Status]='" & drpStatus.SelectedValue & "',"
        sSql += " ModifyBy = '" & UserID & "',"
        sSql += " ModifyDate = '" & Date.Now.ToString("yyyy-MM-dd") & "'"

        sSql += " Where CMID = '" & txtCMID.Text & "'"

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New OleDbConnection(db_SWC.cnPAStr)
            sqlcon.Open()
        End If
        With sqlcom
            .Connection = sqlcon
            .CommandType = CommandType.Text
            .CommandText = sSql
            .ExecuteNonQuery()
        End With
    End Sub


    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        UpdateCM()
    End Sub


    Protected Sub btnAddLabor_Click(sender As Object, e As EventArgs) Handles btnAddLabor.Click
        If txtCMID.Enabled = True Then
            GenID()

            'Repair
            Dim sql0 As String = "INSERT INTO [TB_CMLabor] ([CMID] , LaborCode,DescriptionL,LaborRate ,WorkingHours, CreateBy, CreateDate)"
            sql0 &= "VALUES ('" & txtCMID.Text & "','" & txtLaborCode.Text & "','" & txtDescription.Text & "','" & txtLaborRate.Text & "','" & txtHours.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql0)
            GridView2.DataBind()

            'Main
            Dim sql As String = "INSERT INTO [TB_CorrectiveMaintenance] (CMID, Customer_id , CreateBy, CreateDate"
            sql &= ")"
            sql &= "VALUES ('" & txtCMID.Text & "','" & drpCust.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            txtCMID.Enabled = False

        Else

            Dim sql As String = "INSERT INTO [TB_CMLabor] ([CMID] , LaborCode,DescriptionL,LaborRate ,WorkingHours, CreateBy, CreateDate)"
            sql &= "VALUES ('" & txtCMID.Text & "','" & txtLaborCode.Text & "','" & txtDescription.Text & "','" & txtLaborRate.Text & "','" & txtHours.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            GridView2.DataBind()

        End If
    End Sub

    'Part Required

    Protected Sub drpPartNo_Init(sender As Object, e As EventArgs) Handles drpPartNo.Init
        Dim sqlCon As New SqlConnection(db_SWC.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'SELECT [ItemId],[ItemDesc],[Qty] from [VW_PartsWH] ", conn) ' where SaleId = '" & drpSale.SelectedValue & "'", conn

        sql = "select [ItemId] as id,[ItemDesc]  from [VW_PartsWH] where  ItemId is not null  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_SWC.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpPartNo.Items.Clear()
        drpPartNo.Items.Add(New ListItem("", 1))
        While Rs.Read
            drpPartNo.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpPartNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpPartNo.SelectedIndexChanged
        sql1 += " SELECT  [itemdesc]  ,[qty],UnitPrice  FROM [VW_PartsWH] "
        sql1 += " Where  [ItemId] = '" & drpPartNo.SelectedValue & "'"
        dt1 = db_SWC.GetDataTable(sql1)
        If dt1.Rows.Count > 0 Then
            lblDescriptionPart.Text = dt1.Rows(0)("itemdesc")
            txtQty.Text = dt1.Rows(0)("qty")
            txtUnitPrice.Text = dt1.Rows(0)("UnitPrice")
        End If
    End Sub

    Protected Sub txtQtyUse_TextChanged(sender As Object, e As EventArgs) Handles txtQtyUse.TextChanged
        If txtQtyUse.Text > txtQty.Text Then
            sms.Msg = "QTY not enough !!"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

    End Sub

    Protected Sub btnAddPart_Click(sender As Object, e As EventArgs) Handles btnAddPart.Click
        If txtCMID.Enabled = True Then
            GenID()

            'Part
            Dim sql0 As String = "INSERT INTO [TB_CMPartRequired] ([CMID] ,PartNo,Description,QtyUse ,Price, CreateBy, CreateDate)"
            sql0 &= "VALUES ('" & txtCMID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql0)
            GridView2.DataBind()

            'Main
            Dim sql As String = "INSERT INTO [TB_CorrectiveMaintenance] (CMID, Customer_id , CreateBy, CreateDate"
            sql &= ")"
            sql &= "VALUES ('" & txtCMID.Text & "','" & drpCust.SelectedValue & "'"
            sql &= ",'" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            txtCMID.Enabled = False

        Else

            Dim sql As String = "INSERT INTO [TB_CMPartRequired] ([CMID] ,PartNo,Description,QtyUse ,Price, CreateBy, CreateDate)"
            sql &= "VALUES ('" & txtCMID.Text & "','" & drpPartNo.SelectedValue & "','" & lblDescriptionPart.Text & "','" & txtQtyUse.Text & "','" & txtUnitPrice.Text & "','" & UserID & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"
            dtt = db_SWC.GetDataTable(sql)

            GridView3.DataBind()

        End If
    End Sub

    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        Dim sqlReport As String = " 1=1 "

        '{VW_CM.CMID}="CM17000005"
        If txtCMID.Text <> "" Then
            sqlReport = sqlReport & " and {VW_CM.CMID} = '" & txtCMID.Text & "' "
        End If


        Server.Transfer("CM_LoadReport.aspx?Form=CM&sql=" & sqlReport)
    End Sub

End Class