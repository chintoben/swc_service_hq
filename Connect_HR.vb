﻿
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Data

Public Class Connect_HR

    Public sqlCon As String = "Data Source=10.0.4.20;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_DB_HR") & ";User ID=armth;Password=Kb5r#Ge9Z3M*mQ;packet size=4096;Connect Timeout=4500;Min Pool Size=1;Max Pool Size=200;pooling=false"
    'Public sqlCon As String = "Data Source=10.0.4.20;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_DB_LeaveOnline") & ";User ID=armth;Password=Kb5r#Ge9Z3M*mQ;packet size=4096;Connect Timeout=4500;Min Pool Size=1;Max Pool Size=200;pooling=false"

    Public cnPAStr As String = "Provider='SQLOLEDB.1';User ID='armth';PASSWORD='Kb5r#Ge9Z3M*mQ';Data Source='10.0.4.20';Initial Catalog='DB_HR';"

    Public Function Strcon() As String
        Dim DSx As New Data.DataSet
        Dim Strconx As String

        Strconx = "Provider=sqloledb;Data Source=10.0.4.20;Initial Catalog=DB_HR ;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0; pooling=true; Max Pool Size=200"

        Return Strconx

    End Function

    Public Function Constr2() As String
        Dim sqlconX As String
        sqlconX = "Data Source=10.0.4.20" & ",1433;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_DB_HR") & ";User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=3600; pooling=true; Max Pool Size=200"

        Return sqlconX
    End Function

    Public Function GetDataset(ByVal Strsql As String, _
        Optional ByVal DatasetName As String = "Dataset1", _
        Optional ByVal TableName As String = "Table1") As Data.DataSet
        Dim DA As New OleDbDataAdapter(Strsql, Strcon)
        Dim DS As New Data.DataSet(DatasetName)
        Try
            DA.Fill(DS, TableName)
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        End Try
        Return DS
    End Function

    Public Function GetDataTable(ByVal Strsql As String, _
         Optional ByVal TableName As String = "Table1") As Data.DataTable
        Dim DA As New OleDbDataAdapter(Strsql, Strcon)
        Dim DT As New Data.DataTable(TableName)
        Try
            DA.Fill(DT)
        Catch x1 As Exception
            Err.Raise(60002, , Strsql)
        End Try
        Return DT
    End Function

    'Public Function Constr() As String
    '    Dim sqlconX As String
    '    sqlconX = "Data Source=" & ConfigurationManager.AppSettings("Server") & ",1433;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_SubContractPortal") & ";User ID=armth;Password=edpedp;Connect Timeout=3600; pooling=true; Max Pool Size=200"
    '    Return sqlconX
    'End Function


    Public Function CreateCommand(ByVal Strsql As String) As OleDbCommand
        Dim cmd As New OleDbCommand(Strsql)
        Return cmd
    End Function

    Public Function Execute(ByVal Strsql As String) As Integer
        Dim Cn As New OleDbConnection(Strcon)
        Dim cmd As New OleDbCommand(Strsql, Cn)
        Dim X As Integer
        Try
            Cn.Open()
            X = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Err.Raise(60002, , ex.Message)
            X = -1
        Finally
            Cn.Close()
        End Try
        Return X
    End Function

    Public Function Execute(ByVal Strsql As String, ByVal mt As String) As Integer
        Dim Cn As New OleDbConnection(Strcon)
        Dim cmd As New OleDbCommand(Strsql, Cn)
        Dim X As Integer
        Try
            Cn.Open()
            X = cmd.ExecuteScalar
        Catch ex As Exception
            Err.Raise(60002, , ex.Message)
            X = -1
        Finally
            Cn.Close()
        End Try
        Return X
    End Function


    Public Function ExecuteDataTable(ByVal storedProcedureName As String, ByVal ParamArray arrParam() As SqlParameter) As DataTable
        Dim dt As DataTable

        ' Open the connection 
        Using cnn As New SqlConnection(sqlCon)
            cnn.Open()
            ' Define the command 
            Using cmd As New SqlCommand
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = storedProcedureName
                ' Handle the parameters 
                If arrParam IsNot Nothing Then
                    For Each param As SqlParameter In arrParam
                        cmd.Parameters.Add(param)
                    Next
                End If
                ' Define the data adapter and fill the dataset 
                Using da As New SqlDataAdapter(cmd)
                    dt = New DataTable
                    da.Fill(dt)
                End Using
            End Using
        End Using

        Return dt
    End Function

    Public Function ExecuteDataTable(ByVal storedProcedureName As String) As DataTable
        Dim dt As DataTable

        ' Open the connection 
        Using cnn As New SqlConnection(sqlCon)
            cnn.Open()
            ' Define the command 
            Using cmd As New SqlCommand
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = storedProcedureName
                ' Define the data adapter and fill the dataset 
                Using da As New SqlDataAdapter(cmd)
                    dt = New DataTable
                    da.Fill(dt)
                End Using
            End Using
        End Using

        Return dt
    End Function

    Public Function ExcuteStoredProcedure(ByVal storedProcedureName As String, ByVal ParamArray arrParam() As SqlParameter) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(sqlCon)
        Using cmd As New SqlCommand(storedProcedureName, Cn)
            Try
                If arrParam IsNot Nothing Then
                    For Each param As SqlParameter In arrParam
                        cmd.Parameters.Add(param)
                    Next
                End If
                cmd.CommandType = Data.CommandType.StoredProcedure
                Cn.Open()
                x = cmd.ExecuteScalar
            Catch x1 As Exception
                Err.Raise(60002, , x1.Message)
            Finally
                Cn.Close()
            End Try
        End Using

        Return x
    End Function

    Public Function ExcuteStoredProcedure(ByVal storedProcedureName As String) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(sqlCon)
        Using cmd As New SqlCommand(storedProcedureName, Cn)
            Try
                cmd.CommandType = Data.CommandType.StoredProcedure
                Cn.Open()
                x = cmd.ExecuteScalar
            Catch x1 As Exception
                Err.Raise(60002, , x1.Message)
            Finally
                Cn.Close()
            End Try
        End Using
        Return x
    End Function


End Class