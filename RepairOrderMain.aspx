﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RepairOrderMain.aspx.vb" Inherits="SWC_ServiceSystem.RepairOrderMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Service System</title>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style type="text/css">
        .style1
        {
            height: 39px;
        }
        .fn_ContentTital
        {}
        .style2
        {
            height: 34px;
        }
    </style>

     <script type = "text/javascript">
         function SetTarget() {
             document.forms[0].target = "_blank";
         }
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">

             <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->

           
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
              
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/siam4.jpg" class="navbar-brand"
                    Height="60px" Width="160px" />
                <a class="navbar-brand" href="ServiceMain.aspx">Service System</a>

            </div>
            

            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><asp:Label ID="lblUserName" runat="server" Text="User Name" ></asp:Label><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                   
                    <li>
                           <a  href="ServiceMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                    </li>    
                      
                       <li>
                           <a  href="RepairOrderType.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Repair Order Form</span>
                      </a>
                    </li>    
                    <li> 
                        <a href="RepairOrderMain.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-desktop"></i>
                          <span>Report</span>
                      </a>

                    </li>

                     <%-- <li>
                           <a  href="Customer.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-edit"></i>
                          <span>Customer (New) </span>
                      </a>
                    </li>    --%>

                    <li>
                       <a  href="Authorize.aspx?UserID=<%=Request.QueryString("UserID") %>">
                          <i class="fa fa-fw fa-table"></i>
                          <span>Authorize</span>
                      </a>
                    </li>

                    <li>
                            <a href="Login.aspx"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Repair Order <small></small>
                        </h2>
                        
                       <%-- <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>--%>

                    </div>
                </div>
                <!-- /.row -->
              <div class="row">
                    <div class="col-lg-12">
                        <%--   <div class="panel panel-default">--%>

                <table width="60%" >
            <tr>
                <td align="left" class="style2">

                    &nbsp;</td>
                <td align="left" class="style2">

                    <asp:Label ID="Label9" runat="server" CssClass="fn_ContentTital" 
                        Text="Repair Order Type" ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style2" colspan="4">


                <asp:DropDownList ID="drpType" runat="server" Width="200px" class="form-control"
                        ClientIDMode="Static" Font-Size="Small">
                        <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem Value="RO">Repair Order</asp:ListItem>
                    <asp:ListItem Value="PM">Preventive Maintenance</asp:ListItem>
                    <asp:ListItem Value="CL">Claim</asp:ListItem>
                    <asp:ListItem Value="CA">Campaign</asp:ListItem>
                    </asp:DropDownList>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style2">

                    </td>
                <td align="left" class="style2">

                    <asp:Label ID="Label4" runat="server" CssClass="fn_ContentTital" 
                        Text="Customer : " ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style2" colspan="4">


                <asp:DropDownList ID="drpCust" runat="server" Width="100%" ClientIDMode="Static" class="form-control"
                       Font-Size="Small" >
                    </asp:DropDownList>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style4">

                    &nbsp;</td>
                <td align="left" class="style4">

                    <asp:Label ID="Label7" runat="server" CssClass="fn_ContentTital" 
                        Text="รุ่น : " ToolTip="ชื่อลูกค้า" Width="100%"></asp:Label>
                    </td>
                <td align="left" class="style4" colspan="4">


                  <asp:TextBox ID="txtSeries" runat="server" CssClass="fn_Content" Width="100%"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>


                    </td>
            </tr>       
            <tr>
                <td align="left" class="style1">

                    </td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label5" runat="server" CssClass="fn_ContentTital" 
                        Text="Repair Order Date : " ToolTip="ชื่อลูกค้า" Width="130px"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  <asp:TextBox ID="txtDateRepair" runat="server" Width="200px" Height="30px"></asp:TextBox>

               <%-- <asp:TextBox ID="txtDate" runat="server" Height="30px" Width="200px"></asp:TextBox>--%>


                </td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label6" runat="server" CssClass="fn_ContentTital" 
                        Text="To" ToolTip="ชื่อลูกค้า"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  <asp:TextBox ID="txtDateRepair2" runat="server" CssClass="fn_Content" Width="200px"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>


                    </td>
                <td align="left" class="style1">
                    </td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label65" runat="server" CssClass="fn_ContentTital" 
                        Text="Serial Number :" Font-Size="Small"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  


                  <asp:TextBox ID="txtSerialNumber" runat="server" CssClass="fn_Content" Width="200px"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>

                  


                </td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label80" runat="server" CssClass="fn_ContentTital" 
                        Text="เลขตัวถัง :" Font-Size="Small" Width="60px"></asp:Label>

      
                    </td>
                <td align="left" class="style1">

                  


                  <asp:TextBox ID="txtClassisNumber" runat="server" CssClass="fn_Content" Width="200px"  
                         class="form-control" Font-Size="Small" Height="30px"></asp:TextBox>

                  


                    </td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">

      
                    <asp:Label ID="Label8" runat="server" CssClass="fn_ContentTital" 
                        Text="Status : " ToolTip="ชื่อลูกค้า"></asp:Label>

      
                    </td>
                <td align="left" class="style1">


                <asp:DropDownList ID="drpStatus" runat="server" Width="200px" class="form-control"
                        ClientIDMode="Static" Font-Size="Small">
                        <asp:ListItem>All</asp:ListItem>
                    <asp:ListItem>Open</asp:ListItem>
                    <asp:ListItem>Waiting for spares</asp:ListItem>
                    <asp:ListItem>Close</asp:ListItem>
                    <asp:ListItem>Cancel</asp:ListItem>
                    </asp:DropDownList>


                </td>
                <td align="left" class="style1">

      
                    &nbsp;</td>
                <td align="left" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">

      
                    &nbsp;</td>
                <td align="left" class="style1">


                    &nbsp;</td>
                <td align="left" class="style1">

      
                    &nbsp;</td>
                <td align="left" class="style1">

                    &nbsp;</td>
                <td align="left" class="style1">
                    &nbsp;</td>
            </tr>       
            
          
            <tr>
                <td align="left" class="style13">

                    &nbsp;</td>
                <td align="left" class="style13" colspan="5">

                <asp:Button ID="btnSearch" runat="server" Text="Search" 
                            class="btn btn-info" />
                    &nbsp;
                 <asp:Button ID="btnExport" runat="server" Text="Export" OnClientClick = "SetTarget();"  class="btn btn-default" />	   
                    
                    </td>
            </tr>       
            
          
        </table>

        <br />
        
    
            <asp:GridView ID="GridView" CssClass="footable" runat="server" AutoGenerateColumns="False"
                    Style="max-width: 100%" BackColor="White" BorderColor="#999999" 
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="5" ForeColor="Black" 
                            GridLines="Vertical" Width="100%">
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                    <Columns>
                       <%-- <asp:BoundField DataField="serviceType" HeaderText="serviceType" />--%>
                   <%--     <asp:BoundField DataField="ID" HeaderText="ID" />--%>
                           <asp:HyperLinkField DataNavigateUrlFields="id,UserID" 
                                            DataNavigateUrlFormatString="RepairOrder.aspx?id={0}&UserID={1}" 
                                            DataTextField="ID" HeaderText="ID" 
                                            NavigateUrl="RepairOrder.aspx?id={0}&UserID={1}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>

                                            <ItemStyle Font-Bold="True" />
                                            <ControlStyle ForeColor = "Blue" />
                                        </asp:HyperLinkField>
                   
                        <asp:BoundField DataField="Customer_id" HeaderText="Customer ID" />
                        <asp:BoundField DataField="CustName" HeaderText="Customer Name" />
                        <asp:BoundField DataField="RepairDate" HeaderText="Repair Date" />
                        <asp:BoundField DataField="Receptionnist_Name" HeaderText="Receptionnist_Name" />    
                        <asp:BoundField DataField="DateOpen" HeaderText="Date Open" />
                        <asp:BoundField DataField="DateClose" HeaderText="Date Close" />

                        <asp:BoundField DataField="Stat" HeaderText="Status" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#808080" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>


                       <%-- <asp:BoundField DataField="serviceType" HeaderText="serviceType" />--%>
                    </div>
                </div>
                 
            </div>
            <!-- /.container-fluid -->

            <br />

                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="http://themifycloud.com">Siamwatercraft Copyright © 2017 | All rights Reserved</a></div>
                </div>
                <!--END FOOTER-->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    



    <!-- jQuery -->
   <%-- <script src="js/jquery.js"></script>--%>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


   
     <script>
         // SCRIPT FOR THE MOUSE EVENT.
         function MouseEvents(objRef, evt) {
             if (evt.type == "mouseover") {
                 objRef.style.cursor = 'pointer';
                 objRef.style.backgroundColor = "#EEEED1";
             }
             else {
                 if (evt.type == "mouseout") objRef.style.backgroundColor = "#FFF";
             }
         }
        </script>


      

                   <link href="dropdown/select2.min.css" rel="stylesheet" /> 
                    <script src="dropdown/select2.min.js" type="text/javascript"></script>
                                  
                     <script type="text/javascript">
                         $(document).ready(function () {
                             $("#drpCust").select2({
                             });
                         });
                    </script>



                    <script src="date/jquery-1.7.min.js"></script>
                    <script src="date/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="date/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtDateRepair.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'date/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );

                     $jq('#<%=txtDateRepair2.ClientID%>').datepicker(
                     {
                         showOn: 'button',
                         buttonText: 'Show Date',
                         buttonImageOnly: true,
                         buttonImage: 'date/calendar.gif',
                         dateFormat: 'dd/mm/yy',
                         constrainInput: true
                     }
                     );
                 });     
      </script>
         
                
             

        <%--     <asp:BoundField DataField="ID" HeaderText="ID" />--%><%-- <asp:BoundField DataField="serviceType" HeaderText="serviceType" />--%>

<%--    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
    rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $('[id*=GridView]').footable();
            });
        </script>     --%>
        
           
    </form>
</body>
</html>
